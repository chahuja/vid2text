#!/usr/bin/env python
# 
# The class in this script is a wrapper to Gensim's word2vec
# The function run_example is a brief usage example
#
# Carnegie Mellon University
# author: Salvador Medina
# last update: 2016-04-19
#

import os
import sys
import string
import gensim
import nltk
import numpy as np

class W2VAPI:
    def __init__(self):
        self.model = None
        self.sentences = []
        self.dimension = 300
        
    def load_model(self, model_file_name):
        '''Loads the gensim word2vec binary model file'''
        if not os.path.isfile(model_file_name):
            print 'Model filename does not exist.'
            return None
    
        self.model = gensim.models.Word2Vec.load_word2vec_format(model_file_name, binary=True)
        return self.model    

    def get_gensim_model(self):
        '''Returns the gensim word2vec model'''
        return self.model
    
    def get_word_embedding(self, word):
        '''Return the w2v embedding as a numpy array, the size depends on the loaded model'''
        word_embedding = None
        if word not in self.model:
            word_embedding = np.random.uniform(-0.25, 0.25, self.dimension)
        else:
            word_embedding = self.model[word]
            
        return word_embedding
    
    def get_list_embedding(self, word_list):
        '''Returns the list of embeddings of the given word list'''
        word_list_embedding = []
        for word in word_list:
            word_list_embedding.append(self.get_word_embedding(word))
        return word_list_embedding
    
    def tokenize_sentence(self, sentence):
        '''
        Tokenizes the sentence and removes punctuation marks from the tokens
        Returns the filtered sentence as a list of tokens
        Removes any number or punctuation mark as those are not found within the embeddings
        '''
        # Tokenize
        tokens = nltk.word_tokenize(sentence)

        # Remove punctuation and numbers (removes tokens such as: 25.3, 2/4/53)
        tokens = [str(w).translate(None, string.punctuation+string.digits) for w in tokens]
        # Remove empty tokens
        tokens = [t for t in tokens if len(t)>0]
            
        #Filtered tokens
        return tokens
    
    def get_sentence_embeddings(self, sentence):
        '''Tokenizes the given sentence and returns a list of tuples (token, embedding)'''
        tokens = self.tokenize_sentence(sentence)
        tokens_embeddings = self.get_list_embedding(tokens)
        return zip(tokens, tokens_embeddings)

def run_example():
    
    model_path = '/Users/zal/CMU/QUELCE/Devel/CDClassifier/Data/GoogleNews-vectors-negative300.bin'
    w2v = W2VAPI()
    
    print 'Loading Google News model'
    model = w2v.load_model(model_path)

    sentence = "Is this real? This is insane! He did it at 11:59."    
    print sentence
    
    tokens_embedding = w2v.get_sentence_embeddings(sentence)
    print 'Found %d tokens' % (len(tokens_embedding))
    print '\n'.join([x[0] for x in tokens_embedding])
    
    return w2v

def embedding_init():
    model_path = '/multicomp/datasets/GoogleNews-vectors-negative300.bin'
    w2v = W2VAPI()
    
    print 'Loading Google News model'
    _ = w2v.load_model(model_path)

    return w2v

def get_word_embedding(word,w2v):    
    return w2v.get_word_embedding(word)
