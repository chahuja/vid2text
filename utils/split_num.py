import pickle as pkl
import MySQLdb as sql

movie_list = pkl.load(open('split_list.p','rb'))
db = sql.connect("atlas4.multicomp.cs.cmu.edu","annotator","multicomp","annodb")
cursor = db.cursor()

count_list = [0] * len(movie_list)

for i,set_movie in enumerate(movie_list):
    for movie in set_movie:
        sq1 = 'select count(*) from captions where movie = \'%s\'' %(movie)
        cursor.execute(sq1)
        tuples = cursor.fetchall()
        count_list[i] += int(tuples[0][0])

db.close()

print count_list
