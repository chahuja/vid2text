import os
import MySQLdb as sql
import numpy as np
import pickle as pkl


def import_split_list(split_ratio = [0.6,0.2,0.2],NEW_TRAIN_TEST = 0,SAVE_FLAG = 0, path = './split_list.p'):
    ## import the movies in question
    split_ratio = [0.6,0.2,0.2]
    if (NEW_TRAIN_TEST):
        movieSubsetPath = './dictionaries/movieSubset.p'
        movieList = pkl.load(open(movieSubsetPath,'rb'))
        arr = np.arange(len(movieList))
        np.random.shuffle(arr)

        split_ratio_num = []
        for i in range(len(split_ratio)-1):
            split_ratio_num += [int(np.ceil(split_ratio[i]*len(movieList)))]
        split_ratio_num += [len(movieList) - sum(split_ratio_num)] 

        split_list = []
        sum_num = 0 
        for num in split_ratio_num:
            split_list += [[movieList[arr[i]][0] for i in range(sum_num,sum_num+num)]]
            sum_num+=num

        if (SAVE_FLAG):
            pkl.dump(split_list,open(path,'wb'))

    else:
        split_list = pkl.load(open(path,'rb'))
    return split_list

def import_data():
    movieSplit = import_split_list(path = '../utils/split_list.p')
    db = sql.connect("atlas4.multicomp.cs.cmu.edu","annotator","multicomp","annodb")
    base_path = "/multicomp/datasets"
    dataset = {}
    count = 0
    datasetNames = ['train','test','val']
    
    for set in movieSplit:
        sq1 = 'select * from captions where movie = \'%s\'' %(set[0])
        for movie in set[1:]:
            sq1 +=  ' or  movie = \'%s\''%(movie)
        sq1 += ' ;'
        cursor = db.cursor()    
        cursor.execute(sq1)
        tuples = cursor.fetchall()
        ## Filter the information needed for training
        subData = []
        for tup in tuples:
            subData += [[tup[1],tup[3]]]
        dataset[datasetNames[count]] = subData
        count += 1
    return dataset
