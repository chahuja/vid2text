import os
import pdb
#from preprocessing.vgg_features.keras_vgg import VGG_16, VGG_16_new  #cnn model 
#from keras.optimizers import SGD
import cv2, numpy as np
import MySQLdb as sql
from preprocessing.extract_features import VGG_16
import cPickle as pkl

def get_frames(video_file_path,im_stack):
    video_capture = cv2.VideoCapture(video_file_path)
    ret, cur_frame = video_capture.read()
    im = cv2.resize(cur_frame,(224,224)).astype(np.float32)
    im[:,:,0] = im[:,:,0] - 103.939
    im[:,:,1] = im[:,:,1] - 116.779
    im[:,:,2] = im[:,:,2] - 123.68
    im = im.transpose((2,0,1))
    im = np.expand_dims(im, axis=0)
    total_frames = 0
    im_stack[total_frames,:,:,:] = im
    while ret:
        total_frames += 1
        ret, cur_frame = video_capture.read()
        if not ret:
            break
        im = cv2.resize(cur_frame,(224,224)).astype(np.float32)
        im[:,:,0] = im[:,:,0] - 103.939
        im[:,:,1] = im[:,:,1] - 116.779
        im[:,:,2] = im[:,:,2] - 123.68
        im = im.transpose((2,0,1))
        im = np.expand_dims(im, axis=0)
        im_stack[total_frames,:,:,:] = im
    return im_stack                                                             


#paths
dataset_path = '/multicomp/datasets'
dataset_path_write = '/multicomp/datasets/MontrealVideoAnnotationDataset/feat'
out_folder = 'cnn_features_vid'
# load the vgg model
# path = './preprocessing/vgg_features/vgg16_weights.h5'
# model = VGG_16(path)
# sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
# model.compile(optimizer=sgd, loss='categorical_crossentropy')

# image_model = VGG_16_new(model)
# image_model.compile(optimizer=sgd, loss='categorical_crossentropy')

#Startup VGG_16
model_def = './preprocessing/deploy_features.prototxt'
model = './preprocessing/VGG_ILSVRC_16_layers.caffemodel'
image_model = VGG_16(model_def,model,gpu=1)
pdb.set_trace()
# Connecting to the database
db = sql.connect("atlas4.multicomp.cs.cmu.edu","annotator","multicomp","annodb")
cursor = db.cursor()
#calculate the number of tuples in the table
cursor.execute('select count(*) from allvideos')
numTuples = cursor.fetchall()
numTuples = int(numTuples[0][0])
for i in range(numTuples):
    # get tuple by tuple information
#    cursor = db.cursor()
    cursor.execute('select * from allvideos where id = %d' %(i+1))
    tup = cursor.fetchall()
    for col in tup:
        video_id = int(col[0])
        video_path = col[1]
        movie = col[3]
        dataset = col[4]
        length = int(col[5])
        caption = col[6]
    # declare arrays to store information    
    im_stack = np.zeros((length,3,224,224))
#    final_activations = np.zeros((length,4096))
#    final_label = np.zeros((length,1))

    #Read the video file frame by frame and store it in im_stack
    im_stack = get_frames(os.path.join(dataset_path,video_path),im_stack)
    activations = image_model.batch_predict(im_stack)

    # Write the features in a pickle file
    out_path = os.path.join(dataset_path_write,out_folder,dataset,movie)
    try:
        os.makedirs(out_path)
    except:
        temp = 1
        
    out_path = os.path.join(out_path,".".join(video_path.split('/')[-1].split('.')[0:len(video_path.split('/')[-1].split('.'))-1]))+ '.npy'
    #pdb.set_trace()    
    #pkl.dump(activations,open(out_path,'wb'))
    ## Save the file as npy file
    np.save(out_path, activations)
    # Storing the path in the database
    out_path = os.path.join(dataset_path,out_folder,dataset,movie)
    out_path = os.path.join(out_path,".".join(video_path.split('/')[-1].split('.')[0:len(video_path.split('/')[-1].split('.'))-1]))+ '.npy'
    sq1 = 'insert into cnnfeats (video_id,video_path,cnn_path,num_frames) values (%d,\'%s\',\'%s\',%d)' % (i+1,video_path,out_path,activations.shape[0])
    cursor.execute(sq1)
    db.commit()
#    pdb.set_trace()
