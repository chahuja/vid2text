import os
from pdb import set_trace as stop
from keras.models import Sequential
from keras.layers import Flatten, Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import LSTM
from keras.layers.core import TimeDistributedDense
from keras.optimizers import SGD
from utils.data_split import import_data
from keras.models import model_from_json

import MySQLdb as sql
import re
import pickle as pkl

from utils.data_utils import basic_tokenizer, create_vocabulary, initialize_vocabulary, sentence_to_token_ids, data_to_token_ids
import numpy as np
import re

from time import time

_PAD = "_PAD"
_GO = "_GO"
_EOS = "_EOS"
_UNK = "_UNK"
_START_VOCAB = [_PAD, _GO, _EOS, _UNK]

PAD_ID = 0
GO_ID = 1
EOS_ID = 2
UNK_ID = 3

# Regular expressions used to tokenize.
_WORD_SPLIT = re.compile("([.,!?\"':;)(])")
_DIGIT_RE = re.compile(r"\d")

# Training parameters
NUM_FRAMES = 10

# 0.12 for 4096 dim vectors
## for one hot + cnn features
SIZE_OF_FEAT = 0.42

GPU_MEMORY = 10240 ## in MB

class Data():
    # data
    train = None
    test = None
    val = None
    
    ##vocab dictionaries
    vocab = None 
    rev_vocab = None
    vocab_file_path = None

    ## token_ids 
    token_ids_train = None
    token_ids_val = None
    token_ids_test = None
    
    token_ids_train_path = None
    token_ids_val_path = None
    token_ids_test_path = None

class ProcessedData():

    def __init__(self,rawdata):
        self.train = []
        self.test = []
        self.val = []
        self.decoder = None
        
        self.Rtrain = None
        self.Rtest = None
        self.Rval = None
        
        self.batch_id = 0
        self.valbatch_id = 0
        self.batch_size = 100

        self.vocab = rawdata.vocab
        self.rev_vocab = rawdata.rev_vocab
        self.model = None

    def dump(self,info):
        json_string = self.model.to_json()
        path_to_weights = "weights_%s_epochs_%05d.h5" % (info['model_name'],info['n_epochs'])
        path_to_weights = os.path.join(info['path'],path_to_weights)
        self.model.save_weights(path_to_weights, overwrite = 1)
        out_data = [self.train,
                    self.test,
                    self.val,
                    self.decoder,
                    self.Rtrain,
                    self.Rtest,
                    self.Rval,
                    self.batch_id,
                    self.batch_size,
                    self.vocab,
                    self.rev_vocab,
                    json_string,
                    path_to_weights]
        if not os.path.exists(info['path']):
            os.makedirs(info['path'])
        string = "%s_epochs_%05d.p" % (info['model_name'],info['n_epochs'])
        string = os.path.join(info['path'],string)
        pkl.dump(out_data,open(string,'wb'))

    def load(self,info):
        string = "%s_epochs_%05d.p" % (info['model_name'],info['n_epochs'])
        string = os.path.join(info['path'],string)
        out_data = pkl.load(open(string,'rb'))
        self.train = out_data[0]
        self.test = out_data[1]
        self.val = out_data[2]
        self.decoder = out_data[3]
        self.Rtrain = out_data[4]
        self.Rtest = out_data[5]
        self.Rval = out_data[6]
        self.batch_id = out_data[7]
        self.batch_size = out_data[8]
        self.vocab = out_data[9]
        self.rev_vocab = out_data[10]
        json_string = out_data[11]
        path_to_weights = out_data[12]
        self.model = model_from_json(json_string)
        self.model.load_weights(path_to_weights)

    def split_tokens(self, rawdata):
        for sentence,count in zip(rawdata.token_ids_train,range(len(rawdata.token_ids_train))):
            sentence1 = [GO_ID] + sentence
            sentence2 = sentence + [EOS_ID]
            for w1,w2 in zip(sentence1,sentence2):
                self.train +=[[w1,w2,rawdata.train[count][1]]]

        for sentence,count in zip(rawdata.token_ids_val,range(len(rawdata.token_ids_val))):
            sentence1 = [GO_ID] + sentence
            sentence2 = sentence + [EOS_ID]
            for w1,w2 in zip(sentence1,sentence2):
                self.val +=[[w1,w2,rawdata.val[count][1]]]

        for sentence,count in zip(rawdata.token_ids_test,range(len(rawdata.token_ids_test))):
            sentence1 = [GO_ID] + sentence
            sentence2 = sentence + [EOS_ID]
            for w1,w2 in zip(sentence1,sentence2):
                self.test +=[[w1,w2,rawdata.test[count][1]]]

    def get_cnn_feat(self,db,path):
        cursor = db.cursor()
        sq1 = "select cnn_path from cnnfeatures where video_path = \'%s\'" % (path)
        cursor.execute(sq1)
        tuples = cursor.fetchall()
        cnn_feat = pkl.load(open(tuples[0][0],'rb'))
        return cnn_feat

    def next_batch_onehot(self):
        if not self.Rtrain:
            self.Rtrain = self.train
            np.random.shuffle(self.Rtrain)

        batch = np.zeros((self.batch_size,NUM_FRAMES,len(self.vocab)+4096))
        out_batch = np.zeros((self.batch_size,len(self.vocab)))    
        ## connect to the database
        db = sql.connect("atlas4.multicomp.cs.cmu.edu","annotator","multicomp","annodb")

        print "Creating Batch %d out of %d" %(self.batch_id,len(self.Rtrain)/self.batch_size)
        for count,i in zip(range(self.batch_id*self.batch_size,(self.batch_id+1)*self.batch_size),range(self.batch_size)):
            ## extract cnn_feats
            cnn_feat_raw = self.get_cnn_feat(db,self.train[count][2])
            
            ## subsample frames
            cnn_feat = np.zeros((NUM_FRAMES,4096))
            for j,k in zip(range(NUM_FRAMES),range(0,cnn_feat.shape[0],int(np.ceil(cnn_feat.shape[0]/NUM_FRAMES)))):
                cnn_feat[j,] = cnn_feat_raw[k,]

            ## create a one-hot encoding
            word_feat = np.zeros((1,len(self.vocab)))
            word_feat[0][self.train[count][0]] = 1
            word_feat = np.repeat(word_feat,NUM_FRAMES,axis=0)

            word_feat_out = np.zeros((1,len(self.vocab)))
            word_feat_out[0][self.train[count][1]] = 1

            ## merge features
            feat = np.concatenate((cnn_feat,word_feat),axis=1)

            ## add it to the batch
            batch[i,:,:] = feat

            ## Create the output vector batch
            out_batch[i,:] = word_feat_out

        db.close()

        if (self.batch_size*(self.batch_id+1) > len(self.train)):
            self.batch_id = 0
        else:    
            self.batch_id += 1

        return batch,out_batch
                
    def next_valbatch_onehot(self):
        if not self.Rval:
            self.Rval = self.val
            np.random.shuffle(self.Rval)

        batch = np.zeros((self.batch_size,NUM_FRAMES,len(self.vocab)+4096))
        out_batch = np.zeros((self.batch_size,len(self.vocab)))    
        ## connect to the database
        db = sql.connect("atlas4.multicomp.cs.cmu.edu","annotator","multicomp","annodb")

        for count,i in zip(range(self.valbatch_id*self.batch_size,(self.valbatch_id+1)*self.batch_size),range(self.batch_size)):
            ## extract cnn_feats
            cnn_feat_raw = self.get_cnn_feat(db,self.val[count][2])
            
            ## subsample frames
            cnn_feat = np.zeros((NUM_FRAMES,4096))
            for j,k in zip(range(NUM_FRAMES),range(0,cnn_feat.shape[0],int(np.ceil(cnn_feat.shape[0]/NUM_FRAMES)))):
                cnn_feat[j,] = cnn_feat_raw[k,]

            ## create a one-hot encoding
            word_feat = np.zeros((1,len(self.vocab)))
            word_feat[0][self.val[count][0]] = 1
            word_feat = np.repeat(word_feat,NUM_FRAMES,axis=0)

            word_feat_out = np.zeros((1,len(self.vocab)))
            word_feat_out[0][self.val[count][1]] = 1

            ## merge features
            feat = np.concatenate((cnn_feat,word_feat),axis=1)

            ## add it to the batch
            batch[i,:,:] = feat

            ## Create the output vector batch
            out_batch[i,:] = word_feat_out

        db.close()

        if (self.batch_size*(self.valbatch_id+1) > len(self.val)):
            self.valbatch_id = 0
        else:    
            self.valbatch_id += 1

        return batch,out_batch
        ## TODO self.batch_size = int(np.floor((1.0*GPU_MEMORY)/(NUM_FRAMES*SIZE_OF_FEAT)))


    def lstm(self,in_dim, out_dim):
        self.model = Sequential()
#        self.model.add(Embedding(input_dim = in_dim, output_dim = out_dim, input_shape = (NUM_FRAMES,in_dim), input_length = NUM_FRAMES))
        self.model.add(LSTM(input_dim = in_dim, output_dim=512, batch_input_shape = (self.batch_size,NUM_FRAMES,in_dim), activation='sigmoid', inner_activation='hard_sigmoid'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(out_dim))
        self.model.add(Activation('sigmoid'))
        self.model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

    def lstm_dense(self,in_dim, out_dim):
        self.model = Sequential()
        #        self.model.add(Embedding(input_dim = in_dim, output_dim = 512, input_shape = (self.batch_size,NUM_FRAMES), input_length = NUM_FRAMES))
        self.model.add(TimeDistributedDense(output_dim = 512, input_shape = (NUM_FRAMES,in_dim)))
        self.model.add(LSTM(input_dim = 512, output_dim=512, batch_input_shape = (self.batch_size,NUM_FRAMES,in_dim), activation='sigmoid', inner_activation='hard_sigmoid'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(out_dim))
        self.model.add(Activation('sigmoid'))
        self.model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

## info [model_name,n_epochs,path]
def save_position(data,info):
    if not os.path.exists(info['path']):
        os.makedirs(info['path'])
    string = "%s_epochs_%05d.p" % (info['model_name'],info['n_epochs'])
    string = os.path.join(info['path'],string)
    pkl.dump(data,open(string,'wb'))    

def prev_checkpoint(save_name,save_path):
    ## Check if there is a previous checkpoint    
    FILE_FILTER = "%s_epochs_" % save_name
    
    if os.path.exists(save_path):
        files = os.listdir(os.path.join(save_path))
    else:
        files = []

    for fi in sorted(files,reverse=True):
        if fi.startswith(FILE_FILTER):
            START_POINT = int(fi.split('_')[-1].split('.')[0])+1
            return True, START_POINT
    return False, 1
        
def train_onehot(data,n_epochs = 1000,SAVE_FLAG = 1, save_name = 'model',save_path = './checkpoints'):
    ## check if there is a previous checkpoint for the same model    
    dec,START_POINT = prev_checkpoint(save_name, save_path)
    info = {'model_name':save_name,'n_epochs':START_POINT-1,'path':save_path}        
    ## if there is a checkpoint, load it
    if dec:
        print "\nLoading Old Model\n"
        data.load(info)
    info = {'model_name':save_name,'n_epochs':START_POINT,'path':save_path}        
    for count in range (START_POINT,n_epochs+1):
        batch,out_batch = data.next_batch_onehot()
        valbatch,valout_batch = data.next_valbatch_onehot()
        min_val_loss = np.inf
        while (data.batch_id > 0):
            init_time = time()
            data.model.fit(batch, out_batch, nb_epoch = 1, batch_size = data.batch_size)
            val_loss = data.model.evaluate(valbatch, valout_batch, batch_size = data.batch_size)
            print "VAL Loss: %f" %(val_loss)
            if val_loss < min_val_loss:
                ## save a new model
                if SAVE_FLAG:
                    print "Saving model"
                    info['n_epochs'] = count
                    data.dump(info)                
            batch,out_batch = data.next_batch_onehot()
            valbatch,valout_batch = data.next_valbatch_onehot()
            exit_time = time()
            print "Time (min) : %f" %((exit_time-init_time)/60)

        # if SAVE_FLAG:
        #     if (count-1) % SAVE_FLAG == 0:
        #         info['n_epochs'] = count
        #         #save_position(data,info)
        #         data.dump(info)
        print "\n\n EPOCH %d COMPLETED \n\n" %(count)


def prepare_text_data(rawdata,vocabulary_path,
                 data_path,
                 max_vocabulary_size,
                 overwrite_flag = 0,
                 normalize_digits=True):
    
    ## write data to the datapath
    rawdata_path = os.path.join(data_path,'rawdata')
    train_path = os.path.join(data_path,'traindata')
    val_path = os.path.join(data_path,'valdata')
    test_path = os.path.join(data_path,'testdata')
    
    if (not os.path.exists(rawdata_path)) or overwrite_flag:
        file = open(rawdata_path,'w')
        for string in rawdata.train:
            file.write(string[0]+"\n")
        for string in rawdata.val:
            file.write(string[0]+"\n")
        file.close()

    if (not os.path.exists(train_path)) or overwrite_flag:
        file = open(train_path,'w')
        for string in rawdata.train:
            file.write(string[0]+"\n")
        file.close()

    if (not os.path.exists(val_path)) or overwrite_flag:
        file = open(val_path,'w')
        for string in rawdata.val:
            file.write(string[0]+"\n")
        file.close()

    if (not os.path.exists(test_path)) or overwrite_flag:
        file = open(test_path,'w')
        for string in rawdata.test:
            file.write(string[0]+"\n")
        file.close()
        
    ## create vocabulary
    vocabulary_file = rawdata_path + ".vocab%d" %(max_vocabulary_size)
    rawdata.vocab_file_path = vocabulary_file
    create_vocabulary(vocabulary_file,
                      rawdata_path,
                      max_vocabulary_size)

    ## store the vocab and rev_vocab dictionaries
    rawdata.vocab,rawdata.rev_vocab = initialize_vocabulary(rawdata.vocab_file_path)
    
    ## Training files
    rawdata.token_ids_train_path = train_path + ".ids%d" % max_vocabulary_size
    rawdata.token_ids_val_path = val_path + ".ids%d" % max_vocabulary_size
    rawdata.token_ids_test_path = test_path + ".ids%d" % max_vocabulary_size
    rawdata.token_ids_train = data_to_token_ids(train_path,                    
                                                rawdata.token_ids_train_path,
                                                vocabulary_file,
                                                tokenizer = None,
                                                normalize_digits = True,
                                                overwrite_flag = overwrite_flag)
    rawdata.token_ids_val = data_to_token_ids(val_path,                    
                                              rawdata.token_ids_val_path,
                                              vocabulary_file,
                                              tokenizer = None,
                                              normalize_digits = True,
                                              overwrite_flag = overwrite_flag)
    rawdata.token_ids_test = data_to_token_ids(test_path,                    
                                               rawdata.token_ids_test_path,
                                               vocabulary_file,
                                               tokenizer = None,
                                               normalize_digits = True,
                                               overwrite_flag = overwrite_flag)

    return rawdata

def prepare_vid_data(rawdata):
    ## connect to the database
    db = sql.connect("atlas4.multicomp.cs.cmu.edu","annotator","multicomp","annodb")

    cursor = db.cursor()
    count = 0

    for tup in rawdata.train:
        sq1 = "select cnn_path from cnnfeatures where video_path = \'%s\'" % (tup[1])
        cursor.execute(sq1)
        tuples = cursor.fetchall()
        cnn_feat = pkl.load(open(tuples[0][0],'rb'))

        print count
        count += 1


    for tup in rawdata.val:
        sq1 = "select cnn_path from cnnfeatures where video_path = \'%s\'" % (tup[1])
        cursor.execute(sq1)
        tuples = cursor.fetchall()
        cnn_feat = pkl.load(open(tuples[0][0],'rb'))

        print count
        count += 1


    for tup in rawdata.test:
        sq1 = "select cnn_path from cnnfeatures where video_path = \'%s\'" % (tup[1])
        cursor.execute(sq1)
        tuples = cursor.fetchall()
        cnn_feat = pkl.load(open(tuples[0][0],'rb'))

        print count
        count += 1
    db.close()
    return rawdata
        
if __name__ == '__main__':
    ## Import raw data
    dataset = import_data()
    rawdata = Data()
    rawdata.train = dataset['train']
    rawdata.test = dataset['test']
    if len(dataset) > 2:
        rawdata.val = dataset['val']

    max_vocabulary_size = 40000
    vocabulary_path = "./data/"
    data_path = "./data/"

    rawdata = prepare_text_data(rawdata,vocabulary_path,data_path,
                           max_vocabulary_size,overwrite_flag = 1)

    data = ProcessedData(rawdata)
    data.split_tokens(rawdata)

    ## LSTM-1 onehot
    #data.lstm(len(data.vocab)+4096,len(data.vocab))
    #train_onehot(data,save_name='lstm1layerOnehot',n_epochs=100)

    ## LSTM-1 denseOnhot
    #data.lstm_dense(len(data.vocab)+4096,len(data.vocab))
    #train_onehot(data,save_name='lstm1layerDenseOnehot',n_epochs=100)
