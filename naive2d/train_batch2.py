import os
from pdb import set_trace as stop
from keras.models import Sequential
from keras.layers import Flatten, Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import LSTM
from keras.layers.core import TimeDistributedDense
from keras.optimizers import SGD
from utils.data_split import import_data
from keras.models import model_from_json

import MySQLdb as sql
import re
import pickle as pkl

from utils.data_utils import basic_tokenizer, create_vocabulary, initialize_vocabulary, sentence_to_token_ids, data_to_token_ids
import numpy as np
import re

from time import time

import rawdataLoad
from utils.w2vapi import embedding_init, get_word_embedding

_PAD = "_PAD"
_GO = "_GO"
_EOS = "_EOS"
_UNK = "_UNK"
_START_VOCAB = [_PAD, _GO, _EOS, _UNK]

PAD_ID = 0
GO_ID = 1
EOS_ID = 2
UNK_ID = 3

# Regular expressions used to tokenize.
_WORD_SPLIT = re.compile("([.,!?\"':;)(])")
_DIGIT_RE = re.compile(r"\d")

# Training parameters
NUM_FRAMES = 10

# 0.12 for 4096 dim vectors
## for one hot + cnn features
SIZE_OF_FEAT = 0.42

GPU_MEMORY = 10240 ## in MB

class ProcessedData():

    def __init__(self,rawdata):
        self.train = []
        self.test = []
        self.val = []
        self.decoder = None
        
        self.Rtrain = None
        self.Rtest = None
        self.Rval = None
        
        self.batch_id = 0
        self.valbatch_id = 0
        self.batch_size = 100

        self.vocab = rawdata.vocab
        self.rev_vocab = rawdata.rev_vocab
        self.model = None

    def dump(self,info):
        json_string = self.model.to_json()
        path_to_weights = "weights_%s_epochs_%05d.h5" % (info['model_name'],info['n_epochs'])
        path_to_weights = os.path.join(info['path'],path_to_weights)
        self.model.save_weights(path_to_weights, overwrite = 1)
        out_data = [self.train,
                    self.test,
                    self.val,
                    self.decoder,
                    self.Rtrain,
                    self.Rtest,
                    self.Rval,
                    self.batch_id,
                    self.batch_size,
                    self.vocab,
                    self.rev_vocab,
                    json_string,
                    path_to_weights]
        if not os.path.exists(info['path']):
            os.makedirs(info['path'])
        string = "%s_epochs_%05d.p" % (info['model_name'],info['n_epochs'])
        string = os.path.join(info['path'],string)
        pkl.dump(out_data,open(string,'wb'))

    def load(self,info):
        string = "%s_epochs_%05d.p" % (info['model_name'],info['n_epochs'])
        string = os.path.join(info['path'],string)
        out_data = pkl.load(open(string,'rb'))
        self.train = out_data[0]
        self.test = out_data[1]
        self.val = out_data[2]
        self.decoder = out_data[3]
        self.Rtrain = out_data[4]
        self.Rtest = out_data[5]
        self.Rval = out_data[6]
        self.batch_id = out_data[7]
        self.batch_size = out_data[8]
        self.vocab = out_data[9]
        self.rev_vocab = out_data[10]
        json_string = out_data[11]
        path_to_weights = out_data[12]
        self.model = model_from_json(json_string)
        self.model.load_weights(path_to_weights)

    def split_tokens(self, rawdata):
        for sentence,count in zip(rawdata.token_ids_train,range(len(rawdata.token_ids_train))):
            sentence1 = [GO_ID] + sentence
            sentence2 = sentence + [EOS_ID]
            for w1,w2 in zip(sentence1,sentence2):
                self.train +=[[w1,w2,count]]

        for sentence,count in zip(rawdata.token_ids_val,range(len(rawdata.token_ids_val))):
            sentence1 = [GO_ID] + sentence
            sentence2 = sentence + [EOS_ID]
            for w1,w2 in zip(sentence1,sentence2):
                self.val +=[[w1,w2,count]]

        for sentence,count in zip(rawdata.token_ids_test,range(len(rawdata.token_ids_test))):
            sentence1 = [GO_ID] + sentence
            sentence2 = sentence + [EOS_ID]
            for w1,w2 in zip(sentence1,sentence2):
                self.test +=[[w1,w2,count]]

    def next_batch_w2v(self,rawdata,w2v):
        if not self.Rtrain:
            self.Rtrain = self.train
            np.random.shuffle(self.Rtrain)

        batch = np.zeros((self.batch_size,NUM_FRAMES,300+4096))
        out_batch = np.zeros((self.batch_size,len(self.vocab)))
        
#        if (self.batch_size*(self.batch_id+1) >= len(self.train)):
#            self.batch_id = 0
#        else:    
#            self.batch_id += 1
        
        self.batch_id = (self.batch_id+1)%(len(self.Rtrain)/self.batch_size)
        print self.batch_id
        print "Creating Batch %d out of %d" %(self.batch_id,len(self.Rtrain)/self.batch_size - 1)
        for count,i in zip(range(self.batch_id*self.batch_size,(self.batch_id+1)*self.batch_size),range(self.batch_size)):
            ## extract cnn_feats
            cnn_feat = rawdata.trainVid[i,:,:]

            ## create a w2v encoding
            word_feat = get_word_embedding(rawdata.rev_vocab[self.train[count][0]],w2v)
            word_feat = np.reshape(word_feat,(1,300))
            word_feat = np.repeat(word_feat,NUM_FRAMES,axis=0)

            word_feat_out = np.zeros((1,len(self.vocab)))
            word_feat_out[0][self.train[count][1]] = 1

            ## merge features
            feat = np.concatenate((cnn_feat,word_feat),axis=1)

            ## add it to the batch
            batch[i,:,:] = feat

            ## Create the output vector batch
            out_batch[i,:] = word_feat_out

        return batch,out_batch

    def next_valbatch_w2v(self,rawdata,w2v):
        if not self.Rval:
            self.Rval = self.val
            np.random.shuffle(self.Rval)

        batch = np.zeros((self.batch_size,NUM_FRAMES,300+4096))
        out_batch = np.zeros((self.batch_size,len(self.vocab)))    
        
#        if (self.batch_size*(self.valbatch_id+1) >= len(self.val)):
#            self.valbatch_id = 0
#        else:    
#            self.valbatch_id += 1
    
        self.valbatch_id = (self.valbatch_id+1)%(len(self.Rval)/self.batch_size)
        print self.valbatch_id
        for count,i in zip(range(self.valbatch_id*self.batch_size,(self.valbatch_id+1)*self.batch_size),range(self.batch_size)):
            ## extract cnn_feats
            cnn_feat = rawdata.valVid[i,:,:]
            
            ## create a w2v encoding
            word_feat = np.zeros((1,len(self.vocab)))
            word_feat = get_word_embedding(rawdata.rev_vocab[self.val[count][0]],w2v)
            word_feat = np.reshape(word_feat,(1,300))
            word_feat = np.repeat(word_feat,NUM_FRAMES,axis=0)

            word_feat_out = np.zeros((1,len(self.vocab)))
            word_feat_out[0][self.val[count][1]] = 1

            ## merge features
            feat = np.concatenate((cnn_feat,word_feat),axis=1)

            ## add it to the batch
            batch[i,:,:] = feat

            ## Create the output vector batch
            out_batch[i,:] = word_feat_out

        return batch,out_batch
        ## TODO self.batch_size = int(np.floor((1.0*GPU_MEMORY)/(NUM_FRAMES*SIZE_OF_FEAT)))


    def lstm(self,in_dim, out_dim):
        self.model = Sequential()
#        self.model.add(Embedding(input_dim = in_dim, output_dim = out_dim, input_shape = (NUM_FRAMES,in_dim), input_length = NUM_FRAMES))
        self.model.add(LSTM(input_dim = in_dim, output_dim=512, batch_input_shape = (self.batch_size,NUM_FRAMES,in_dim), activation='sigmoid', inner_activation='hard_sigmoid'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(out_dim))
        self.model.add(Activation('sigmoid'))
        self.model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

    def lstm_dense(self,in_dim, out_dim):
        self.model = Sequential()
        #        self.model.add(Embedding(input_dim = in_dim, output_dim = 512, input_shape = (self.batch_size,NUM_FRAMES), input_length = NUM_FRAMES))
        self.model.add(TimeDistributedDense(output_dim = 512, input_shape = (NUM_FRAMES,in_dim)))
        self.model.add(LSTM(input_dim = 512, output_dim=512, batch_input_shape = (self.batch_size,NUM_FRAMES,in_dim), activation='sigmoid', inner_activation='hard_sigmoid'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(out_dim))
        self.model.add(Activation('sigmoid'))
        self.model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

    def lstm_dense2(self,in_dim, out_dim):
        self.model = Sequential()
        #        self.model.add(Embedding(input_dim = in_dim, output_dim = 512, input_shape = (self.batch_size,NUM_FRAMES), input_length = NUM_FRAMES))
        self.model.add(TimeDistributedDense(output_dim = 512, input_shape = (NUM_FRAMES,in_dim)))
        self.model.add(LSTM(input_dim = 512, output_dim=512, batch_input_shape = (self.batch_size,NUM_FRAMES,in_dim), activation='sigmoid', inner_activation='hard_sigmoid',return_sequences = True))
        self.model.add(Dropout(0.5))
        self.model.add(LSTM(input_dim = 512, output_dim=512, batch_input_shape = (self.batch_size,NUM_FRAMES,512), activation='sigmoid', inner_activation='hard_sigmoid'))
        self.model.add(Dense(out_dim))
        self.model.add(Activation('sigmoid'))
        self.model.compile(loss='categorical_crossentropy', optimizer='rmsprop')
        
        
def prev_checkpoint(save_name,save_path):
    ## Check if there is a previous checkpoint    
    FILE_FILTER = "%s_epochs_" % save_name
    
    if os.path.exists(save_path):
        files = os.listdir(os.path.join(save_path))
    else:
        files = []

    for fi in sorted(files,reverse=True):
        if fi.startswith(FILE_FILTER):
            START_POINT = int(fi.split('_')[-1].split('.')[0])+1
            return True, START_POINT
    return False, 1

def train_w2v(data,rawdata,w2v,n_epochs = 1000,SAVE_FLAG = 1, save_name = 'model',save_path = './checkpoints'):
    ## check if there is a previous checkpoint for the same model    
    dec,START_POINT = prev_checkpoint(save_name, save_path)
    info = {'model_name':save_name,'n_epochs':START_POINT-1,'path':save_path}        
    ## if there is a checkpoint, load it
    if dec:
        print "\nLoading Old Model\n"
        data.load(info)
    info = {'model_name':save_name,'n_epochs':START_POINT,'path':save_path}        
    for count in range (START_POINT,n_epochs+1):
        batch,out_batch = data.next_batch_w2v(rawdata,w2v)
        valbatch,valout_batch = data.next_valbatch_w2v(rawdata,w2v)
        min_val_loss = np.inf
        while (data.batch_id > 0):
            init_time = time()
            data.model.fit(batch, out_batch, nb_epoch = 1, batch_size = data.batch_size)
            val_loss = data.model.evaluate(valbatch, valout_batch, batch_size = data.batch_size)
            print "VAL Loss: %f" %(val_loss)
            if val_loss < min_val_loss:
                ## save a new model
                if SAVE_FLAG:
                    print "Saving model"
                    info['n_epochs'] = count
                    data.dump(info)                
            batch,out_batch = data.next_batch_w2v(rawdata,w2v)
            valbatch,valout_batch = data.next_valbatch_w2v(rawdata,w2v)
            exit_time = time()
            print "Time (min) : %f" %((exit_time-init_time)/60)

        # if SAVE_FLAG:
        #     if (count-1) % SAVE_FLAG == 0:
        #         info['n_epochs'] = count
        #         #save_position(data,info)
        #         data.dump(info)
        print "\n\n EPOCH %d COMPLETED \n\n" %(count)

if __name__ == "__main__":

    ## If rawdata is already loaded, move on to training tasks
    try:
        rawdata
    except NameError:
        rawdata = rawdataLoad.main(NUM_FRAMES)
    data = ProcessedData(rawdata)
    data.split_tokens(rawdata)

    ## Load the w2vec model if not already loaded
    try:
        w2v
    except NameError:
        w2v = embedding_init()

    ## LSTM-1 onehot
    #data.lstm(len(data.vocab)+4096,len(data.vocab))
    #train_onehot(data,save_name='lstm1layerOnehot',n_epochs=100)

    ## LSTM- w2vec 300 dim 1 layer
    #data.lstm_dense(300+4096,len(data.vocab))
    #train_w2v(data,rawdata,w2v,save_name='lstm1layerw2v',n_epochs=100)    

    ## LSTM- w2vec 300 dim 2 layer
    data.lstm_dense2(300+4096,len(data.vocab))
    train_w2v(data,rawdata,w2v,save_name='lstm2layerw2v',n_epochs=100)    
