import os
from pdb import set_trace as stop
from keras.models import Sequential
from keras.layers import Flatten, Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import LSTM
from keras.layers.core import TimeDistributedDense
from keras.optimizers import SGD
from utils.data_split import import_data
from keras.models import model_from_json

import MySQLdb as sql
import re
import pickle as pkl

from utils.data_utils import basic_tokenizer, create_vocabulary, initialize_vocabulary, sentence_to_token_ids, data_to_token_ids
import numpy as np
import re

from time import time

_PAD = "_PAD"
_GO = "_GO"
_EOS = "_EOS"
_UNK = "_UNK"
_START_VOCAB = [_PAD, _GO, _EOS, _UNK]

PAD_ID = 0
GO_ID = 1
EOS_ID = 2
UNK_ID = 3

# Regular expressions used to tokenize.
_WORD_SPLIT = re.compile("([.,!?\"':;)(])")
_DIGIT_RE = re.compile(r"\d")

# Training parameters
NUM_FRAMES = 10

# 0.12 for 4096 dim vectors
## for one hot + cnn features
SIZE_OF_FEAT = 0.42

GPU_MEMORY = 10240 ## in MB

class Data():
    # data
    train = None
    test = None
    val = None
    
    ##vocab dictionaries
    vocab = None 
    rev_vocab = None
    vocab_file_path = None

    ## token_ids 
    token_ids_train = None
    token_ids_val = None
    token_ids_test = None
    
    token_ids_train_path = None
    token_ids_val_path = None
    token_ids_test_path = None


## Split and tokenize data
def prepare_text_data(rawdata,vocabulary_path,
                 data_path,
                 max_vocabulary_size,
                 overwrite_flag = 0,
                 normalize_digits=True):
    
    ## write data to the datapath
    rawdata_path = os.path.join(data_path,'rawdata')
    train_path = os.path.join(data_path,'traindata')
    val_path = os.path.join(data_path,'valdata')
    test_path = os.path.join(data_path,'testdata')
    
    if (not os.path.exists(rawdata_path)) or overwrite_flag:
        file = open(rawdata_path,'w')
        for string in rawdata.train:
            file.write(string[0]+"\n")
        for string in rawdata.val:
            file.write(string[0]+"\n")
        file.close()

    if (not os.path.exists(train_path)) or overwrite_flag:
        file = open(train_path,'w')
        for string in rawdata.train:
            file.write(string[0]+"\n")
        file.close()

    if (not os.path.exists(val_path)) or overwrite_flag:
        file = open(val_path,'w')
        for string in rawdata.val:
            file.write(string[0]+"\n")
        file.close()

    if (not os.path.exists(test_path)) or overwrite_flag:
        file = open(test_path,'w')
        for string in rawdata.test:
            file.write(string[0]+"\n")
        file.close()
        
    ## create vocabulary
    vocabulary_file = rawdata_path + ".vocab%d" %(max_vocabulary_size)
    rawdata.vocab_file_path = vocabulary_file
    create_vocabulary(vocabulary_file,
                      rawdata_path,
                      max_vocabulary_size)

    ## store the vocab and rev_vocab dictionaries
    rawdata.vocab,rawdata.rev_vocab = initialize_vocabulary(rawdata.vocab_file_path)
    
    ## Training files
    rawdata.token_ids_train_path = train_path + ".ids%d" % max_vocabulary_size
    rawdata.token_ids_val_path = val_path + ".ids%d" % max_vocabulary_size
    rawdata.token_ids_test_path = test_path + ".ids%d" % max_vocabulary_size
    rawdata.token_ids_train = data_to_token_ids(train_path,                    
                                                rawdata.token_ids_train_path,
                                                vocabulary_file,
                                                tokenizer = None,
                                                normalize_digits = True,
                                                overwrite_flag = overwrite_flag)
    rawdata.token_ids_val = data_to_token_ids(val_path,                    
                                              rawdata.token_ids_val_path,
                                              vocabulary_file,
                                              tokenizer = None,
                                              normalize_digits = True,
                                              overwrite_flag = overwrite_flag)
    rawdata.token_ids_test = data_to_token_ids(test_path,                    
                                               rawdata.token_ids_test_path,
                                               vocabulary_file,
                                               tokenizer = None,
                                               normalize_digits = True,
                                               overwrite_flag = overwrite_flag)

    return rawdata

def prepare_vid_data(rawdata,NUM_FRAMES):
    ## connect to the database
    db = sql.connect("atlas4.multicomp.cs.cmu.edu","annotator","multicomp","annodb")
    
    cursor = db.cursor()
#    count = 0

    rawdata.trainVid = np.zeros((len(rawdata.train),NUM_FRAMES,4096))
    init_time = time()
    for tup,count in zip(rawdata.train,range(len(rawdata.train))):
        sq1 = "select cnn_path from cnnfeatures where video_path = \'%s\'" % (tup[1])
        cursor.execute(sq1)
        tuples = cursor.fetchall()
        cnn_feat_raw = pkl.load(open(tuples[0][0],'rb'))
        ## subsample frames
        #cnn_feat = np.zeros((NUM_FRAMES,4096))
        for j,k in zip(range(NUM_FRAMES),range(0,cnn_feat_raw.shape[0],int(np.ceil(cnn_feat_raw.shape[0]/NUM_FRAMES)))):
            rawdata.trainVid[count,j,] = cnn_feat_raw[k,]
        print count
        if (count%100==0): end_time = time(); print "TIME(min) %f"%((end_time-init_time)/60); init_time = time()

    rawdata.valVid = np.zeros((len(rawdata.val),NUM_FRAMES,4096))
    init_time = time()
    for tup,count in zip(rawdata.val,range(len(rawdata.val))):
        sq1 = "select cnn_path from cnnfeatures where video_path = \'%s\'" % (tup[1])
        cursor.execute(sq1)
        tuples = cursor.fetchall()
        cnn_feat_raw = pkl.load(open(tuples[0][0],'rb'))
        ## subsample frames
        #cnn_feat = np.zeros((NUM_FRAMES,4096))
        for j,k in zip(range(NUM_FRAMES),range(0,cnn_feat_raw.shape[0],int(np.ceil(cnn_feat_raw.shape[0]/NUM_FRAMES)))):
            rawdata.valVid[count,j,] = cnn_feat_raw[k,]
        print count
        if (count%100==0): end_time = time(); print "TIME(min) %f"%((end_time-init_time)/60); init_time = time()

    rawdata.testVid = np.zeros((len(rawdata.test),NUM_FRAMES,4096))
    init_time = time()
    for tup,count in zip(rawdata.test,range(len(rawdata.test))):
        sq1 = "select cnn_path from cnnfeatures where video_path = \'%s\'" % (tup[1])
        cursor.execute(sq1)
        tuples = cursor.fetchall()
        cnn_feat_raw = pkl.load(open(tuples[0][0],'rb'))
        ## subsample frames
        #cnn_feat = np.zeros((NUM_FRAMES,4096))
        for j,k in zip(range(NUM_FRAMES),range(0,cnn_feat_raw.shape[0],int(np.ceil(cnn_feat_raw.shape[0]/NUM_FRAMES)))):
            rawdata.testVid[count,j,] = cnn_feat_raw[k,]
        print count
        if (count%100==0): end_time = time(); print "TIME(min) %f"%((end_time-init_time)/60); init_time = time()
        

    db.close()
    return rawdata
        
def main(NUM_FRAMES):
    ## Import raw data
    dataset = import_data()
    rawdata = Data()
    rawdata.train = dataset['train']
    rawdata.test = dataset['test']
    if len(dataset) > 2:
        rawdata.val = dataset['val']

    max_vocabulary_size = 40000
    vocabulary_path = "./data/"
    data_path = "./data/"

    rawdata = prepare_text_data(rawdata,vocabulary_path,data_path,
                                max_vocabulary_size,overwrite_flag = 1)

    cnn_feat_path = './feats'
    if (not os.path.exists(cnn_feat_path)):
        rawdata = prepare_vid_data(rawdata,NUM_FRAMES)
        print "Raw data Prepared. Writing to file. This may take a few hours"
        np.save(os.path.join(cnn_feat_path,'cnnfeat_train.npy'),rawdata.trainVid)
        np.save(os.path.join(cnn_feat_path,'cnnfeat_val.npy'),rawdata.valVid)
        np.save(os.path.join(cnn_feat_path,'cnnfeat_test.npy'),rawdata.testVid)
    else:
        print "Loading saved data"
        rawdata.trainVid = np.load((os.path.join(cnn_feat_path,'cnnfeat_train.npy')))
        rawdata.valVid = np.load((os.path.join(cnn_feat_path,'cnnfeat_val.npy')))
        rawdata.testVid = np.load((os.path.join(cnn_feat_path,'cnnfeat_test.npy')))
                       
                                                              
    return rawdata
