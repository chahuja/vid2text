import os
import sys
import subprocess
import pdb

base_path = '/multicomp/datasets/MPII/'
in_path = base_path + 'avi'
out_path = base_path + 'avi_low_res'

count1 = 1
count2 = 1
for movie in os.listdir(in_path):
    count2 = 1
    subprocess.call(['mkdir', os.path.join(out_path, movie)])
    for clip in os.listdir(os.path.join(in_path,movie)):
        subprocess.call(['ffmpeg', '-i', os.path.join(in_path,movie,clip), '-vf', 'scale=720:306', os.path.join(out_path,movie,clip)])
        print str(count1) + '.' + str(count2)
        count2 = count2 + 1
    count1 = count1 + 1
#    pdb.set_trace()
        
