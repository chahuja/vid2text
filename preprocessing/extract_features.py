import sys
import os.path
import argparse
import pdb
import numpy as np
from scipy.misc import imread, imresize
import scipy.io

import cPickle as pickle

# parser = argparse.ArgumentParser()
# parser.add_argument('--caffe',
#                     help='path to caffe installation')
# parser.add_argument('--model_def',
#                     help='path to model definition prototxt')
# parser.add_argument('--model',
#                     help='path to model parameters')
# parser.add_argument('--files',
#                     help='path to a file contsining a list of images')
# parser.add_argument('--gpu',
#                     action='store_true',
#                     help='whether to use gpu training')
# parser.add_argument('--out',
#                     help='name of the pickle file where to store the features')

# args = parser.parse_args()

# if args.caffe:
#     caffepath = args.caffe + '/python'
#     sys.path.append(caffepath)

import caffe

class VGG_16():

    def predict(self,in_data):
        """
        Get the features for a batch of data using network
        
        Inputs:
        in_data: data batch
        """

        out = self.net.forward(**{self.net.inputs[0]: in_data})
        features = out[self.net.outputs[0]]
        return features


    def batch_predict(self,im_stack):
        """
        Get the features for all images from filenames using a network
            
        Inputs:
        filenames: a list of names of image files

        Returns:
        an array of feature vectors for the images in that file
        """

        N, C, H, W = self.net.blobs[self.net.inputs[0]].data.shape
        F = self.net.blobs[self.net.outputs[0]].data.shape[1]
        Nf = im_stack.shape[0]
#        Hi, Wi, _ = imread(filenames[0]).shape
        allftrs = np.zeros((Nf, F))

        for i in range(0, Nf, N):
            in_data = np.zeros((N, C, H, W), dtype=np.float32)

            batch_range = range(i, min(i+N, Nf))
            Nb = len(batch_range)

            batch_images = np.zeros((Nb, 3, H, W))
            
            # insert into correct place
            in_data[0:len(batch_range), :, :, :] = im_stack[batch_range[0]:batch_range[0]+len(batch_range)]
            
            # predict features
            ftrs = self.predict(in_data)
        
            for j in range(len(batch_range)):
                allftrs[i+j,:] = ftrs[j,:]

            print 'Done %d/%d files' % (i+len(batch_range), im_stack.shape[0])

        return allftrs

    def __init__(self,model_def,model,gpu):
        if gpu:
            caffe.set_mode_gpu()
        else:
            caffe.set_mode_cpu()

        self.net = caffe.Net(model_def, model, caffe.TEST)        

#allftrs = batch_predict(filenames, net)
