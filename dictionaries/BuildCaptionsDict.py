#!/usr/bin/env python

import os
import glob
import cPickle as pickle
from os.path import join
import cv2
#import NLPAnalysis as nlpa

all_srt_path = '/multicomp/datasets/MontrealVideoAnnotationDataset/M-VAD/srt_files/all_srt'
video_base_path = '/multicomp/datasets/MontrealVideoAnnotationDataset/DVDtranscription'
dict_filename = './all_captions_dict.p'
inv_dict_filename = './all_captions_inv_dict.p'
verbs_dict_filename = './all_captions_verbs_dict.p'

captions_dict = {}
captions_inv_dict = {}
verbs_dict = {}


def get_total_frames(video_file_path):
    video_capture = cv2.VideoCapture(video_file_path)
    ret, cur_frame = video_capture.read()
    total_frames = 0
    while ret:
        total_frames += 1
        ret, cur_frame = video_capture.read()

    return total_frames

count = 0
for srt_file in glob.glob(join(all_srt_path,'*.srt')):
    movie_name = os.path.splitext(os.path.basename(srt_file))[0]
    all_lines = open(join(all_srt_path, srt_file),'r').readlines()
    for i in range(0,len(all_lines),4):
        file_name = all_lines[i].strip()
        caption = all_lines[i+2].strip()
        total_frames = get_total_frames(os.path.join(video_base_path,movie_name,'video',file_name+'.avi'))
        count +=1

        #Captions dict Key:VideoFileName Value:Caption
        captions_dict[file_name]=[caption,total_frames]
        
        #Captions Inv Index  Key:Caption Value:Video FileName
#        captions_inv_dict[caption]=file_name
        #Inverted dict based on verbs   Key: lemmatizedVerb Value: [(filename, caption), ...]
#        caption_verbs = nlpa.extract_lemmatized_verbs(caption.decode('utf8'))
        # for verb in caption_verbs:
        #     if verb not in verbs_dict:
        #         verbs_dict[verb] = [(file_name, caption)]
        #     else:
        #         verbs_dict[verb].append((file_name, caption))
    print movie_name
    print count
    count = 0    

pickle.dump(captions_dict,open(dict_filename, 'wb'))
#pickle.dump(captions_inv_dict,open(inv_dict_filename, 'wb'))
#pickle.dump(verbs_dict, open(verbs_dict_filename, 'wb'))
