import MySQLdb as sql
import pdb
import os
import pickle as pkl

# Montreal

## connect to the database
db = sql.connect("atlas4.multicomp.cs.cmu.edu","annotator","multicomp","annodb")

################################################################
# +------------+--------------+------+-----+---------+-------+ #
# | Field      | Type         | Null | Key | Default | Extra | #
# +------------+--------------+------+-----+---------+-------+ #
# | video_path | varchar(255) | NO   | PRI | NULL    |       | #
# | movie      | text         | YES  |     | NULL    |       | #
# | dataset    | text         | NO   |     | NULL    |       | #
# | length     | int(11)      | NO   |     | NULL    |       | #
# | status     | int(8)       | NO   |     | 0       |       | #
# | caption    | text         | NO   |     | NULL    |       | #
# +------------+--------------+------+-----+---------+-------+ #
################################################################

base_path = "/multicomp/datasets"

# # Load all the captions from the pickle file
# all_capt = pkl.load(open('all_captions_dict_montreal.p','rb'))

# # Set the base paths
# dataset = "MontrealVideoAnnotationDataset"
# video_path_base = "DVDtranscription"
# count = 1
# for clip in all_capt:
#     movie = clip.split('_DVS')[0]
#     caption = all_capt[clip][0]
#     # process caption
#     temp_caption = caption.split('\'')
#     caption = '\\\''.join(temp_caption)
#     #pdb.set_trace()
#     length = all_capt[clip][1]
#     video_path = os.path.join(dataset,video_path_base,movie,'video',clip+'.avi')
#     cursor = db.cursor()
#     sq1 = "insert into allvideos (video_path,movie,dataset,length,caption) values (\'"+video_path +"\',\'"+movie+"\',\'"+dataset+"\',"+str(length)+",\'"+caption+"\');"
#     cursor.execute(sq1)
#     #print sq1
#     db.commit()
#     print count
#     count+=1
#     #pdb.set_trace()
# db.close()

# Load all the captions from the pickle file
all_capt = pkl.load(open('mpii_dict.p','rb'))

# Set the base paths
dataset = "MPII"
video_path_base = "avi"
count = 1
for clip in all_capt:
    movie = clip[0:len(clip)-26]
    caption = all_capt[clip][0]
    # process caption
    temp_caption = caption.split('\'')
    caption = '\\\''.join(temp_caption)
    #pdb.set_trace()
    length = all_capt[clip][1]
    video_path = os.path.join(dataset,video_path_base,movie,clip+'.avi')
    cursor = db.cursor()
    sq1 = "insert into allvideos (video_path,movie,dataset,length,caption) values (\'"+video_path +"\',\'"+movie+"\',\'"+dataset+"\',"+str(length)+",\'"+caption+"\');"
    cursor.execute(sq1)
    #print sq1
    db.commit()
    print count
    count+=1
    #pdb.set_trace()
db.close()
