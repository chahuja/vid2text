import MySQLdb as sql
import pdb
import os
import pickle as pkl
import numpy as np
import operator

def genFreqTable(cursor,movie_list):
    freq_table = {}
    for movie in movie_list:
        sq1 = "select count(*) from captions where movie = \'%s\'" %(movie)
        cursor.execute(sq1)
        tuples = cursor.fetchall()
        freq_table[movie] = int(tuples[0][0])
    return freq_table
        
## connect to the database
db = sql.connect("atlas4.multicomp.cs.cmu.edu","annotator","multicomp","annodb")

################################################################
# +------------+--------------+------+-----+---------+-------+ #
# | Field      | Type         | Null | Key | Default | Extra | #
# +------------+--------------+------+-----+---------+-------+ #
# | video_path | varchar(255) | NO   | PRI | NULL    |       | #
# | movie      | text         | YES  |     | NULL    |       | #
# | dataset    | text         | NO   |     | NULL    |       | #
# | length     | int(11)      | NO   |     | NULL    |       | #
# | status     | int(8)       | NO   |     | 0       |       | #
# | caption    | text         | NO   |     | NULL    |       | #
# +------------+--------------+------+-----+---------+-------+ #
################################################################

base_path = "/multicomp/datasets"

# get the list of movies
cursor = db.cursor()
cursor.execute("select distinct movie from captions")
tuples = cursor.fetchall()
movie_list = [tup[0] for tup in tuples]

# get a frequency list for all movies
freq_table = genFreqTable(cursor,movie_list)
sorted_table = sorted(freq_table.items(), key=operator.itemgetter(1))
#sorted_table.reverse()

# stop at 16k
count = 0
i = 0
for tup in sorted_table:
    count = count + tup[1]
    i += 1
    if count > 16000:
        print count
        break

pkl.dump(sorted_table[0:i],open('movieSubset.p','wb'))
final_table = sorted_table[0:i]

sq1 = "select * from captions where movie = \'%s\'" % (final_table[0][0])
for i in range(1,len(final_table)):
    sq1 = sq1 + " or movie = \'%s\'" %(final_table[i][0])

sq1 = sq1 + " order by action;"
pdb.set_trace()

cursor = db.cursor() 
cursor.execute(sq1)
tuples = cursor.fetchall()

count = 0
for tup in tuples:
    temp_caption = tup[1].split('\'')
    temp_caption = '\\\''.join(temp_caption)
    sq1 = "insert into annotationtask (id,captionid,text,video_name,video_path,action,movie,dataset,status,batch_id) values (%d,%d,\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',-1,%d);" %(count+1,int(tup[0]),temp_caption,tup[2],tup[3],tup[4],tup[5],tup[6],count/100 + 1)
    count = count + 1
    cursor.execute(sq1)
    db.commit()
#    pdb.set_trace()
db.close()
