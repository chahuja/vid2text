#!/bin/bash

# Download the grid corpus and extract the files

if [ $# -eq 0 ] ; then
    echo "No directory provided. downloading data in default directory ../dataset"
    dir='../dataset'
else
    dir=$1
fi

if [ ! -d "$dir" ] ; then
    echo "Directory not found. Making Directory"
    mkdir -p $dir
fi

wget http://mattmahoney.net/dc/enwik8.zip
unzip enwik8.zip -d $dir
rm enwik8.zip
