
# coding: utf-8

# In[1]:

import os
import numpy as np
import random
import string
import tensorflow as tf
from tqdm import tqdm


# In[2]:

def read_data(filename):
  f = open(filename)
  return tf.compat.as_str(f.read())
  f.close()
  
filename = 'dataset/input_1million.txt'
text = read_data(filename)
print('Data size %d' % len(text))


# In[3]:

valid_size = 1000
valid_text = text[:valid_size]
train_text = text[valid_size:]
train_size = len(train_text)
print(train_size, train_text[:64])
print(valid_size, valid_text[:64])


# In[4]:

# Create Vocabulary
vocabulary_size = 0
vocab = {}
rev_vocab = []
for char in text:
  if char not in vocab.keys():
    vocab[char] = vocabulary_size
    rev_vocab += char
    vocabulary_size += 1

def char2id(char):
  if char in vocab.keys():
    return vocab[char]
  else:
    print('Unexpected character: %s' % char)
    return 0
    
def id2char(dictid):
  if dictid >=0 :
    return rev_vocab[dictid]
  else:
    print ('Invalid ID: %d' % dictid)
    return 0
print(char2id('a'), char2id('z'), char2id(' '), char2id('ï'))
print(id2char(1), id2char(26), id2char(0))


# In[5]:

batch_size=100
num_unrollings=100

class BatchGenerator(object):
  def __init__(self, text, batch_size, num_unrollings):
    self._text = text
    self._text_size = len(text)
    self._batch_size = batch_size
    self._num_unrollings = num_unrollings
    segment = self._text_size // batch_size
    self._cursor = [ offset * segment for offset in range(batch_size)]
    self._last_batch = self._next_batch()
  
  def _next_batch(self):
    """Generate a single batch from the current cursor position in the data."""
    batch = np.zeros(shape=(self._batch_size, vocabulary_size), dtype=np.float)
    for b in range(self._batch_size):
      batch[b, char2id(self._text[self._cursor[b]])] = 1.0
      self._cursor[b] = (self._cursor[b] + 1) % self._text_size
    return batch
  
  def next(self):
    """Generate the next array of batches from the data. The array consists of
    the last batch of the previous array, followed by num_unrollings new ones.
    """
    batches = [self._last_batch]
    for step in range(self._num_unrollings):
      batches.append(self._next_batch())
    self._last_batch = batches[-1]
    return batches

def characters(probabilities):
  """Turn a 1-hot encoding or a probability distribution over the possible
  characters back into its (most likely) character representation."""
  return [id2char(c) for c in np.argmax(probabilities, 1)]

def batches2string(batches):
  """Convert a sequence of batches back into their (most likely) string
  representation."""
  s = [''] * batches[0].shape[0]
  for b in batches:
    s = [''.join(x) for x in zip(s, characters(b))]
  return s

train_batches = BatchGenerator(text, batch_size, num_unrollings)
valid_batches = BatchGenerator(valid_text, 1, 1)

print(batches2string(train_batches.next()))
print(batches2string(train_batches.next()))
print(batches2string(valid_batches.next()))
print(batches2string(valid_batches.next()))


# In[6]:

def logprob(predictions, labels):
  """Log-probability of the true labels in a predicted batch."""
  predictions[predictions < 1e-10] = 1e-10
  return np.sum(np.multiply(labels, -np.log(predictions))) / labels.shape[0]

def sample_distribution(distribution):
  """Sample one element from a distribution assumed to be an array of normalized
  probabilities.
  """
  r = random.uniform(0, 1)
  s = 0
  for i in range(len(distribution)):
    s += distribution[i]
    if s >= r:
      return i
  return len(distribution) - 1

def sample(prediction, argmax=False):
  """Turn a (column) prediction into 1-hot encoded samples."""
  if argmax:
    index = np.argmax(prediction)
  else:
    index = sample_distribution(prediction[0])
  p = np.zeros(shape=[1, vocabulary_size], dtype=np.float)
  p[0, index] = 1.0
  return p

def random_distribution():
  """Generate a random column of probabilities."""
  b = np.random.uniform(0.0, 1.0, size=[1, vocabulary_size])
  return b/np.sum(b, 1)[:,None]


# In[ ]:

num_nodes = 512

graph = tf.Graph()
with graph.as_default():
  
  # Parameters:  
  # Input gate: input, previous output, and bias.
  high1 = np.sqrt(6.0/(num_nodes+vocabulary_size))
  high2 = np.sqrt(6.0/(num_nodes*2))
  xz = tf.Variable(tf.truncated_normal([vocabulary_size, num_nodes], -high1, high1))
  sz = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -high2, high2))
  bz = tf.Variable(tf.zeros([1, num_nodes]))
  # Forget gate: input, previous output, and bias.
  xr = tf.Variable(tf.truncated_normal([vocabulary_size, num_nodes], -high1, high1))
  sr = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -high2, high2))
  br = tf.Variable(tf.zeros([1, num_nodes]))
  # Memory cell: input, state and bias.                             
  xh = tf.Variable(tf.truncated_normal([vocabulary_size, num_nodes], -high1, high1))
  sh = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -high1, high2))
  bh = tf.Variable(tf.zeros([1, num_nodes]))

  # Variables saving state across unrollings.
  saved_state = tf.Variable(tf.zeros([batch_size, num_nodes]), trainable=False)
  # Classifier weights and biases.
  w = tf.Variable(tf.truncated_normal([num_nodes, vocabulary_size], -high1, high1))
  b = tf.Variable(tf.zeros([vocabulary_size]))
  
  # Definition of the cell computation.
  def gru_cell(x,s):
    z = tf.sigmoid(tf.matmul(x, xz) + tf.matmul(s, sz) + bz)
    r = tf.sigmoid(tf.matmul(x, xr) + tf.matmul(s, sr) + br)
    h = tf.tanh(tf.matmul(x, xh) + tf.matmul(tf.mul(s,r), sh) + bh)
    return tf.add(tf.mul((tf.ones_like(z) - z), h), tf.mul(z,s))
  
  # Input data.
  train_data = list()
  for _ in range(num_unrollings + 1):
    train_data.append(
      tf.placeholder(tf.float32, shape=[batch_size,vocabulary_size]))
  train_inputs = train_data[:num_unrollings]
  train_labels = train_data[1:]  # labels are inputs shifted by one time step.

  # Unrolled GRU loop.
  outputs = list()
  state = saved_state
  for i in train_inputs:
    state = gru_cell(x=i, s=state)
    outputs.append(state)

  # State saving across unrollings.
  with tf.control_dependencies([saved_state.assign(state)]):
    # Classifier.
    logits = tf.nn.xw_plus_b(tf.concat(0, outputs), w, b)
    loss = tf.reduce_mean(
      tf.nn.softmax_cross_entropy_with_logits(
        logits, tf.concat(0, train_labels)))

  # Optimizer
  global_step = tf.Variable(0)
  #learning_rate = tf.Variable(0.002, trainable=False) ## Fixed learning rate
  #momentum = tf.Variable(0.01, trainable=False) ## Fixed learning rate
  learning_rate = tf.train.exponential_decay(10.0, global_step, 1000, 0.97, staircase=True)
  #decay_rms = tf.Variable(0.95, trainable=False)
  optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
  #optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate, decay=decay_rms)
  #optimizer = tf.train.MomentumOptimizer(learning_rate = learning_rate, momentum=momentum)
  gradients, v = zip(*optimizer.compute_gradients(loss))
  gradients, _ = tf.clip_by_global_norm(gradients, 1.25)
  optimizer = optimizer.apply_gradients(
    zip(gradients, v), global_step=global_step)
  
  # Predictions.
  train_prediction = tf.nn.softmax(logits)
  
  # Sampling and validation eval: batch 1, no unrolling.
  sample_input = tf.placeholder(tf.float32, shape=[1, vocabulary_size])
  saved_sample_state = tf.Variable(tf.zeros([1, num_nodes]))
  reset_sample_state = tf.group(saved_sample_state.assign(tf.zeros([1, num_nodes])))
  sample_state = gru_cell(
    sample_input, saved_sample_state)
  with tf.control_dependencies([saved_sample_state.assign(sample_state)]):
    sample_prediction = tf.nn.softmax(tf.nn.xw_plus_b(sample_state, w, b))


# In[ ]:

num_steps = 50001
summary_frequency = 100

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.5)
with tf.Session(graph=graph, config=tf.ConfigProto(gpu_options=gpu_options)) as session:
  tf.initialize_all_variables().run()
  print('Initialized')
  mean_loss = 0
  for step in tqdm(range(num_steps)):
    batches = train_batches.next()
    feed_dict = dict()
    for i in range(num_unrollings + 1):
      feed_dict[train_data[i]] = batches[i]
    _, l, predictions, lr = session.run(
      [optimizer, loss, train_prediction, learning_rate], feed_dict=feed_dict)
    mean_loss += l
    if step % summary_frequency == 0:
      if step > 0:
        mean_loss = mean_loss / summary_frequency
      # The mean loss is an estimate of the loss over the last few batches.
      print(
        'Average loss at step %d: %f learning rate: %f' % (step, mean_loss, lr))
      mean_loss = 0
      labels = np.concatenate(list(batches)[1:])
      print('Minibatch perplexity: %.2f' % float(
        np.exp(logprob(predictions, labels))))
      if step % (summary_frequency * 10) == 0:
        # Generate some samples.
        print('=' * 80)
        for _ in range(2):
          feed = sample(random_distribution())
          sentence = characters(feed)[0]
          reset_sample_state.run()
          print('=' * 80)
          for _ in range(300):
            #prediction = sample_prediction.eval({sample_input: feed})
            prediction, state, saved_state = session.run([sample_prediction, sample_state, saved_sample_state],
                                    {sample_input:feed})
            v2 = state
            b2 = saved_state
            #pdb.set_trace()
            v1 = state
            b1 = saved_state
            feed = sample(prediction, argmax=False)
            sentence += characters(feed)[0]
          print(sentence)
        print('=' * 80)
      # Measure validation set perplexity.
      reset_sample_state.run()
      valid_logprob = 0
      for _ in range(valid_size):
        b = valid_batches.next()
        predictions = sample_prediction.eval({sample_input: b[0]})
        valid_logprob = valid_logprob + logprob(predictions, b[1])
      print('Validation set perplexity: %.2f' % float(np.exp(
        valid_logprob / valid_size)))

