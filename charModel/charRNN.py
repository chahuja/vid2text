
# coding: utf-8

# # Todo
# - multiple layer gru generalization
# - variable batch_size
# - same model for prediction
# - multiple hidden layers
# - epoch properply

# In[ ]:

# These are all the modules we'll be using later. Make sure you can import them
# before proceeding further.
from __future__ import print_function
import os
import numpy as np
import random
import string
import tensorflow as tf
import zipfile
from six.moves import range
from six.moves.urllib.request import urlretrieve
import pdb
import sys
import argparse
import pickle as pkl
from tqdm import tqdm
from time import time
from tensorflow.python.ops.rnn_cell import GRUCell
import seaborn as sb


# In[ ]:

def plot_tensor(l, batch_num=0):
  try:
    mat = np.vstack(tuple([l[i][batch_num,:] for i in range(len(l))]))
  except:
    print("Batch Number out of scope")
    return 0
  sb.heatmap(mat)
  sb.plt.show()


# In[ ]:

def read_data(filename):
  f = open(filename)
  return tf.compat.as_str(f.read())
  f.close()
  
filename = 'input_1million.txt'
text = read_data(filename)
print('Data size %d' % len(text))


# In[ ]:

# Create Vocabulary
vocabulary_size = 0
vocab = {}
rev_vocab = []
for char in text:
  if char not in vocab.keys():
    vocab[char] = vocabulary_size
    rev_vocab += char
    vocabulary_size += 1


# In[ ]:

def char2id(char):
  if char in vocab.keys():
    return vocab[char]
  else:
    print('Unexpected character: %s' % char)
    return 0
    
def id2char(dictid):
  if dictid >=0 :
    return rev_vocab[dictid]
  else:
    print ('Invalid ID: %d' % dictid)
    return 0
print(char2id('a'), char2id('z'), char2id(' '), char2id('ï'))
print(id2char(1), id2char(26), id2char(0))


# In[ ]:

class BatchGenerator(object):
  def __init__(self, text, batch_size, num_unrollings):
    self._text = text
    self._text_size = len(text)
    self._batch_size = batch_size
    if(isinstance(num_unrollings,(list))):
      self._num_unrollings = max(num_unrollings)
    else:
      self._num_unrollings = num_unrollings
    segment = self._text_size // batch_size
    self._cursor = [ offset * segment for offset in range(batch_size)]
    self._last_batch = self._next_batch()
    self._num_epochs = 0
  
  def _next_batch(self):
    """Generate a single batch from the current cursor position in the data."""
    batch = np.zeros(shape=(self._batch_size, vocabulary_size), dtype=np.float)
    for b in range(self._batch_size):
      batch[b, char2id(self._text[self._cursor[b]])] = 1.0
      self._cursor[b] = (self._cursor[b] + 1) % self._text_size
      if (self._cursor[b] == 0 and self._batch_size > 1):
        print("\n NUM-EPOCHS: %d\n" % self._num_epochs)
        self._num_epochs += 1
        #pdb.set_trace()
    return batch
  
  def next(self):
    """Generate the next array of batches from the data. The array consists of
    the last batch of the previous array, followed by num_unrollings new ones.
    """
    batches = [self._last_batch]
    for step in range(self._num_unrollings):
      batches.append(self._next_batch())
    self._last_batch = batches[-1]
    return batches

def characters(probabilities):
  """Turn a 1-hot encoding or a probability distribution over the possible
  characters back into its (most likely) character representation."""
  return [id2char(c) for c in np.argmax(probabilities, 1)]

def batches2string(batches):
  """Convert a sequence of batches back into their (most likely) string
  representation."""
  s = [''] * batches[0].shape[0]
  for b in batches:
    s = [''.join(x) for x in zip(s, characters(b))]
  return s

def load_data(args):
  train_batches = BatchGenerator(train_text, args.batch_size, args.num_unrollings)
  print(batches2string(train_batches.next()))
  print(batches2string(train_batches.next()))
  print(batches2string(valid_batches.next()))
  print(batches2string(valid_batches.next()))
  
  return train_batches, valid_batches


# In[ ]:

def logprob(predictions, labels):
  """Log-probability of the true labels in a predicted batch."""
  predictions[predictions < 1e-10] = 1e-10
  return np.sum(np.multiply(labels, -np.log(predictions))) / labels.shape[0]

def sample_distribution(distribution):
  """Sample one element from a distribution assumed to be an array of normalized
  probabilities.
  """
  r = random.uniform(0, 1)
  s = 0
  for i in range(len(distribution)):
    s += distribution[i]
    if s >= r:
      return i
  return len(distribution) - 1

def sample(prediction):
  """Turn a (column) prediction into 1-hot encoded samples."""
  p = np.zeros(shape=[1, vocabulary_size], dtype=np.float)
  p[0, sample_distribution(prediction[0])] = 1.0
  return p

def random_distribution():
  """Generate a random column of probabilities."""
  b = np.random.uniform(0.0, 1.0, size=[1, vocabulary_size])
  return b/np.sum(b, 1)[:,None]


# In[ ]:

class Model(object):
  def __init__(self,args):
    self.graph = tf.Graph()
    with self.graph.as_default():
      # GRU
      zx, zh, zb = [tf.Variable(tf.truncated_normal([args.input_dim, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.truncated_normal([args.rnn_size, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.zeros([1, args.rnn_size]))]
      # debugging 
      self.zx = zx
      rx, rh, rb = [tf.Variable(tf.truncated_normal([args.input_dim, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.truncated_normal([args.rnn_size, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.zeros([1, args.rnn_size]))]
      cx, ch, cb = [tf.Variable(tf.truncated_normal([args.input_dim, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.truncated_normal([args.rnn_size, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.zeros([1, args.rnn_size]))]
      
      ## gru second layer weights hardcoded
      zx2, zh2, zb2 = [tf.Variable(tf.truncated_normal([args.rnn_size, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.truncated_normal([args.rnn_size, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.zeros([1, args.rnn_size]))]
      rx2, rh2, rb2 = [tf.Variable(tf.truncated_normal([args.rnn_size, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.truncated_normal([args.rnn_size, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.zeros([1, args.rnn_size]))]
      cx2, ch2, cb2 = [tf.Variable(tf.truncated_normal([args.rnn_size, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.truncated_normal([args.rnn_size, args.rnn_size], -0.1, 0.1)),                    tf.Variable(tf.zeros([1, args.rnn_size]))]
      
      # Variables saving output across unrollings
      saved_h = tf.Variable(tf.zeros([args.batch_size,args.rnn_size]), trainable=False)
      saved_h2 = tf.Variable(tf.zeros([args.batch_size,args.rnn_size]), trainable=False)
      # Layer to reduce to vocabulary space
      w = tf.Variable(tf.truncated_normal([args.rnn_size,args.input_dim], -0.1, 0.1))
      b = tf.Variable(tf.zeros([args.input_dim]))
      
      def gru_cell(x_,h_):
        H = tf.concat(1,[x_]+[h_])
        Wz = tf.concat(0,[zx]+[zh])
        Wr = tf.concat(0,[rx]+[rh])
        Wc = tf.concat(0,[cx]+[ch])
        
        z = tf.sigmoid(tf.matmul(H,Wz)+zb)
        r = tf.sigmoid(tf.matmul(H,Wr)+rb)
        hcap = tf.tanh(tf.matmul(tf.concat(1,[x_]+[tf.mul(r,h_)]),Wc) + cb)
        h = tf.mul(tf.ones_like(z) - z,h_) + tf.mul(z,hcap) 
        return h
      
      def gru_cell1(x_,h_):
        z = tf.sigmoid(tf.matmul(x_,zx) + tf.matmul(h_,zh) + zb)
        r = tf.sigmoid(tf.matmul(x_,rx) + tf.matmul(h_,rh) + rb)
        hcap = tf.tanh(tf.matmul(x_,cx) + tf.matmul(tf.mul(r,h_),ch) + cb)
        h = tf.mul(tf.ones_like(z)-z,h_) + tf.mul(z,hcap)
        return h
      
      # 2 Layer Gru
      def gru_cell2(x_,h_,h2):
        # Layer 1
        H = tf.concat(1,[x_]+[h_])
        Wz = tf.concat(0,[zx]+[zh])
        Wr = tf.concat(0,[rx]+[rh])
        Wc = tf.concat(0,[cx]+[ch])
        
        z = tf.sigmoid(tf.matmul(H,Wz)+zb)
        r = tf.sigmoid(tf.matmul(H,Wr)+rb)
        hcap = tf.tanh(tf.matmul(tf.concat(1,[x_]+[tf.mul(r,h_)]),Wc) + cb)
        h = tf.mul(tf.ones_like(z) - z,h_) + tf.mul(z,hcap) 
        
        #layer 2
        H2 = tf.concat(1,[h]+[h2])
        Wz2 = tf.concat(0,[zx2]+[zh2])
        Wr2 = tf.concat(0,[rx2]+[rh2])
        Wc2 = tf.concat(0,[cx2]+[ch2])
        
        z2 = tf.sigmoid(tf.matmul(H2,Wz2)+zb2)
        r2 = tf.sigmoid(tf.matmul(H2,Wr2)+rb2)
        hcap2 = tf.tanh(tf.matmul(tf.concat(1,[h]+[tf.mul(r2,h2)]),Wc2) + cb2)
        h3 = tf.mul(tf.ones_like(z2) - z2,h2) + tf.mul(z2,hcap2) 
        
        return h, h3
        
      # Choose the kind of cell
      if (args.model == 'gru'):
        self.cell_fn = gru_cell
      elif (args.model == 'gru1'):
        self.cell_fn = gru_cell1
      elif (args.model == 'gru2'):
        self.cell_fn = gru_cell2
      elif (args.model == 'gruTF'):
        self.cell_fn = GRUCell(num_units=args.rnn_size)
      else:
        raise Exception("model type not supported: {}".format(args.model))
  
      # get inputs
      self.train_data = list()
      for _ in range(args.num_unrollings + 1):
        self.train_data.append(
          tf.placeholder(tf.float32, shape=[args.batch_size,vocabulary_size]))
      self.train_inputs = self.train_data[:args.num_unrollings]
      self.train_labels = self.train_data[1:]
      #self.train_inputs = [tf.placeholder(tf.float32, shape=[None,args.input_dim]) for _ in range(args.num_unrollings)]
      #self.train_labels = [tf.placeholder(tf.float32, shape=[None,args.input_dim]) for _ in range(args.num_unrollings)]
      
      # Connections
      self.h_list = list()
      h = saved_h
      h2 = saved_h2
      for x in self.train_inputs:
        #h = self.cell_fn(x,h)
        h2, h = self.cell_fn(x,h2,h)
        self.h_list.append(h)
      
      # Saving state across unrollings
      with tf.control_dependencies([saved_h.assign(h),saved_h2.assign(h2)]):
        # get loss
        logits = tf.nn.xw_plus_b(tf.concat(0,self.h_list), w, b)
        self.loss = tf.reduce_mean(
          tf.nn.softmax_cross_entropy_with_logits(
            logits,tf.concat(0,self.train_labels)))
      
      # Optimizer
      self.global_step = tf.Variable(0, trainable=False)
      self.learning_rate = tf.train.exponential_decay(
        args.learning_rate, self.global_step, 10000, args.decay_rate, staircase = True)
      #self.optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss, global_step=self.global_step)
      self.optimizer = tf.train.GradientDescentOptimizer(self.learning_rate).minimize(self.loss,global_step=self.global_step)
      #gradients, v = zip(*self.optimizer.compute_gradients(self.loss))
      #gradients, _ = tf.clip_by_global_norm(gradients,1.25)
      #self.optimizer = self.optimizer.apply_gradients(
      #  zip(gradients,v), global_step=self.global_step)
        
      #global_step = tf.Variable(0)
      #self.learning_rate = tf.train.exponential_decay(
      #  10.0, global_step, 5000, 0.1, staircase=True)
      #self.optimizer = tf.train.GradientDescentOptimizer(self.learning_rate)
      #gradients, v = zip(*self.optimizer.compute_gradients(self.loss))
      #gradients, _ = tf.clip_by_global_norm(gradients, 1.25)
      #self.optimizer = self.optimizer.apply_gradients(
      #  zip(gradients, v), global_step=global_step)
      
      # Predictions
      self.train_prediction = tf.nn.softmax(logits)
      
      # Sample Prediction
      self.sample_input = tf.placeholder(tf.float32, [1,args.input_dim])
      saved_sample_h = tf.Variable(tf.zeros([1,args.rnn_size]))
      saved_sample_h2 = tf.Variable(tf.zeros([1,args.rnn_size]))
      self.reset_sample_h = tf.group(saved_sample_h.assign(tf.zeros([1,args.rnn_size])),
                                    saved_sample_h2.assign(tf.zeros([1,args.rnn_size])))
      #sample_h = self.cell_fn(self.sample_input, saved_sample_h)
      sample_h2, sample_h = self.cell_fn(self.sample_input, saved_sample_h2, saved_sample_h)
      with tf.control_dependencies([saved_sample_h.assign(sample_h),
                                   saved_sample_h2.assign(sample_h2)]):
        self.sample_prediction = tf.nn.softmax(tf.nn.xw_plus_b(sample_h,w,b))


# In[ ]:

def train(args):
  args.input_dim = vocabulary_size
  
  # Loading Data
  train_batches = BatchGenerator(text,batch_size=args.batch_size,num_unrollings=args.num_unrollings)
  #print(batches2string(train_batches.next()))
  #print(batches2string(train_batches.next()))
  #pdb.set_trace()
  valid_batches = text[0]
  print("Data Loaded")
  
  model = Model(args)
  print("Model Loaded")
  
  print("training started")
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=args.frac_gpu)
  
  with tf.Session(graph=model.graph,config=tf.ConfigProto(gpu_options=gpu_options)) as session:
    tf.initialize_all_variables().run()
    print("Model Initialized")
    
    saver = tf.train.Saver()
    print("Saver Initialized")
    mean_loss = 0
    
    for step in tqdm(range(args.num_epochs)):
      batches = train_batches.next()
      feed_dict = dict()
      for i in range(args.num_unrollings+1):
        #feed_dict[model.train_inputs[i]] = batches[i]
        #feed_dict[model.train_labels[i]] = batches[i+1]
        feed_dict[model.train_data[i]] = batches[i]
        #tf.Print(model.train_data[i],[model.train_data[i]])
        #pdb.set_trace()
    
      _, loss, predictions, lr, outs = session.run(
        [model.optimizer,model.loss,model.train_prediction,model.learning_rate, model.h_list],
        feed_dict=feed_dict)
      mean_loss += loss
      
      if step % args.save_every == 0:
        if step > 0:
          mean_loss = mean_loss / args.save_every
        
        # Debugging
        #pdb.set_trace()
        w_new = model.zx.eval()
        try:
          print(np.mean(w_new - w_old))
        except:
          print("Edge Case")
        w_old = model.zx.eval()
        
        print("Average loss at step %d: %f learning rate: %f" % (step,mean_loss,lr))
        mean_loss = 0
        labels = np.concatenate(list(batches)[1:])
        print("MiniBatch perplexity: %.2f" % float(
          np.exp(logprob(predictions[:labels.shape[0]],labels))))
        
        if step % (args.save_every * 1) == 0:
          # Generate Some Samples
          print("=" * 80)
          feed = np.zeros([1,vocabulary_size])
          feed[0][char2id(valid_batches)] = 1
          sentence = characters(feed)[0]
          model.reset_sample_h.run()
          for _ in range(400):
            prediction = model.sample_prediction.eval({model.sample_input:feed})
            feed = sample(prediction)
            sentence += characters(feed)[0]
          print(sentence)
        print("=" * 80)


# In[1]:

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='data/tinyshakespeare',
                        help='data directory containing input.txt')
    parser.add_argument('--save_dir', type=str, default='save',
                        help='directory to store checkpointed models')
    parser.add_argument('--rnn_size', type=int , default=512,
                        help='size of RNN hidden state') 
    parser.add_argument('--num_layers', type=int, default=2,
                        help='number of layers in the RNN')
    parser.add_argument('--model', type=str, default='gru2',
                        help='rnn, gru, lstm, gridlstm, gridgru')
    parser.add_argument('--batch_size', type=int, default=100,
                        help='minibatch size')
    parser.add_argument('--num_epochs', type=int, default=5000,
                        help='number of epochs')
    parser.add_argument('--save_every', type=int, default=100,
                        help='save frequency')
    parser.add_argument('--grad_clip', type=float, default=5.,
                        help='clip gradients at this value')
    parser.add_argument('--learning_rate', type=float, default=0.01,
                        help='learning rate')
    parser.add_argument('--decay_rate', type=float, default=0.99,
                        help='decay rate for rmsprop')
    parser.add_argument('--model_name', type=str, default='modelCpk',
                        help='checkpoint file name')
    parser.add_argument('--num_unrollings', type=int , default=100,
                        help='number of time stamps')
    parser.add_argument('--num_dim', type=int, default=1,
                        help='number of dimesions of the grid lstm')
    parser.add_argument('--frac_gpu', type=float, default=0.5,
                        help='fraction of gpu to use')
    parser.add_argument('--optimizer', type=str, default='adam',
                        help='the optimizer')
    args = parser.parse_args()
      
    train(args)


# In[ ]:

if __name__ == '__main__':
    main()


# In[ ]:



