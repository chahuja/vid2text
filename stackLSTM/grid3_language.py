
# coding: utf-8

# # TODO
#     - include dropout
#     - include gradient clipping
#     - include masking
#     - Multi-layer Support

# In[1]:

# These are all the modules we'll be using later. Make sure you can import them
# before proceeding further.
from __future__ import print_function
import os
import numpy as np
import random
import string
import tensorflow as tf
import zipfile
from six.moves import range
from six.moves.urllib.request import urlretrieve
import pdb
import sys
import argparse


# In[2]:

url = 'http://mattmahoney.net/dc/'

def maybe_download(filename, expected_bytes):
  """Download a file if not present, and make sure it's the right size."""
  if not os.path.exists(filename):
    filename, _ = urlretrieve(url + filename, filename)
  statinfo = os.stat(filename)
  if statinfo.st_size == expected_bytes:
    print('Found and verified %s' % filename)
  else:
    print(statinfo.st_size)
    raise Exception(
      'Failed to verify ' + filename + '. Can you get to it with a browser?')
  return filename

filename = maybe_download('text8.zip', 31344016)


# In[3]:

def read_data(filename):
  f = zipfile.ZipFile(filename)
  for name in f.namelist():
    return tf.compat.as_str(f.read(name))
  f.close()
  
text = read_data(filename)
print('Data size %d' % len(text))


# Create a small validation set.

# In[4]:

valid_size = 1000
valid_text = text[:valid_size]
train_text = text[valid_size:]
train_size = len(train_text)
print(train_size, train_text[:64])
print(valid_size, valid_text[:64])


# Utility functions to map characters to vocabulary IDs and back.

# In[5]:

vocabulary_size = len(string.ascii_lowercase) + 1 # [a-z] + ' '
first_letter = ord(string.ascii_lowercase[0])

def char2id(char):
  if char in string.ascii_lowercase:
    return ord(char) - first_letter + 1
  elif char == ' ':
    return 0
  else:
    print('Unexpected character: %s' % char)
    return 0
  
def id2char(dictid):
  if dictid > 0:
    return chr(dictid + first_letter - 1)
  else:
    return ' '

print(char2id('a'), char2id('z'), char2id(' '), char2id('ï'))
print(id2char(1), id2char(26), id2char(0))


# Function to generate a training batch for the LSTM model.

# TODO use batch_size and num_unrollings from args
# ------------------------
# ------------------------

# In[6]:

batch_size=64
num_unrollings=15

class BatchGenerator(object):
  def __init__(self, text, batch_size, num_unrollings):
    self._text = text
    self._text_size = len(text)
    self._batch_size = batch_size
    self._num_unrollings = num_unrollings
    segment = self._text_size // batch_size
    self._cursor = [ offset * segment for offset in range(batch_size)]
    self._last_batch = self._next_batch()
  
  def _next_batch(self):
    """Generate a single batch from the current cursor position in the data."""
    batch = np.zeros(shape=(self._batch_size, vocabulary_size), dtype=np.float)
    for b in range(self._batch_size):
      batch[b, char2id(self._text[self._cursor[b]])] = 1.0
      self._cursor[b] = (self._cursor[b] + 1) % self._text_size
    return batch
  
  def next(self):
    """Generate the next array of batches from the data. The array consists of
    the last batch of the previous array, followed by num_unrollings new ones.
    """
    batches = [self._last_batch]
    for step in range(self._num_unrollings):
      batches.append(self._next_batch())
    self._last_batch = batches[-1]
    return batches

def characters(probabilities):
  """Turn a 1-hot encoding or a probability distribution over the possible
  characters back into its (most likely) character representation."""
  return [id2char(c) for c in np.argmax(probabilities, 1)]

def batches2string(batches):
  """Convert a sequence of batches back into their (most likely) string
  representation."""
  s = [''] * batches[0].shape[0]
  for b in batches:
    s = [''.join(x) for x in zip(s, characters(b))]
  return s

train_batches = BatchGenerator(train_text, batch_size, num_unrollings)
valid_batches = BatchGenerator(valid_text, 1, 1)

print(batches2string(train_batches.next()))
print(batches2string(train_batches.next()))
print(batches2string(valid_batches.next()))
print(batches2string(valid_batches.next()))


# In[7]:

def logprob(predictions, labels):
  """Log-probability of the true labels in a predicted batch."""
  predictions[predictions < 1e-10] = 1e-10
  return np.sum(np.multiply(labels, -np.log(predictions))) / labels.shape[0]

def sample_distribution(distribution):
  """Sample one element from a distribution assumed to be an array of normalized
  probabilities.
  """
  r = random.uniform(0, 1)
  s = 0
  for i in range(len(distribution)):
    s += distribution[i]
    if s >= r:
      return i
  return len(distribution) - 1

def sample(prediction):
  """Turn a (column) prediction into 1-hot encoded samples."""
  p = np.zeros(shape=[1, vocabulary_size], dtype=np.float)
  p[0, sample_distribution(prediction[0])] = 1.0
  return p

def random_distribution():
  """Generate a random column of probabilities."""
  b = np.random.uniform(0.0, 1.0, size=[1, vocabulary_size])
  return b/np.sum(b, 1)[:,None]


# Grid LSTM model
# ---------------

# In[8]:

"""
TODO
args(
rnn_size: the dimensions of a given LSTM cell
vocalbulary_size: input size -- to be replaced with input_size
batch_size: Batch Size
num_dim: number of dimentions of the grid LSTM model

)
"""


# In[9]:

class Model(object):
  def __init__(self,args):
    self.graph = tf.Graph()
    self.loss = None
    with self.graph.as_default():

      # Parameters:
      # Initialized by a normal distribution truncated b/w 0.1 and -0.1
      # Input gate: input, previous output, and bias.
      ix = [tf.Variable(tf.truncated_normal([args.input_dim[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      im = [tf.Variable(tf.truncated_normal([args.rnn_size[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      ic = [tf.Variable(tf.truncated_normal([args.rnn_size[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      ib = [tf.Variable(tf.zeros([1, args.rnn_size[j]])) for j in range(args.num_dim)]
      # Forget gate: input, previous output, and bias.
      fx = [tf.Variable(tf.truncated_normal([args.input_dim[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      fm = [tf.Variable(tf.truncated_normal([args.rnn_size[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      fc = [tf.Variable(tf.truncated_normal([args.rnn_size[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      fb = [tf.Variable(tf.zeros([1, args.rnn_size[j]])) for j in range(args.num_dim)]
      # Memory cell: input, state and bias.                             
      cx = [tf.Variable(tf.truncated_normal([args.input_dim[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      cm = [tf.Variable(tf.truncated_normal([args.rnn_size[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      cc = [tf.Variable(tf.truncated_normal([args.rnn_size[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      cb = [tf.Variable(tf.zeros([1, args.rnn_size[j]])) for j in range(args.num_dim)]
      # Output gate: input, previous output, and bias.
      ox = [tf.Variable(tf.truncated_normal([args.input_dim[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      om = [tf.Variable(tf.truncated_normal([args.rnn_size[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      oc = [tf.Variable(tf.truncated_normal([args.rnn_size[j], args.rnn_size[j]], -0.1, 0.1)) for j in range(args.num_dim)]
      ob = [tf.Variable(tf.zeros([1, args.rnn_size[j]])) for j in range(args.num_dim)]
      # Variables saving state across unrollings.
      saved_output = [tf.Variable(tf.zeros([args.batch_size, args.rnn_size[j]]), trainable=False) for j in range(args.num_dim)]
      saved_state = [tf.Variable(tf.zeros([args.batch_size, args.rnn_size[j]]), trainable=False) for j in range(args.num_dim)]
      # Classifier weights and biases.
      w = [tf.Variable(tf.truncated_normal([args.rnn_size[dim], args.input_dim[dim]], -0.1, 0.1)) for dim in range(args.num_dim)]
      b = [tf.Variable(tf.zeros([args.input_dim[dim]])) for dim in range(args.num_dim)]
      
      # Definition of the cell computation.
      def gridlstm_cell(i_,o_,state_,num_dim = 1):
        """GridLSTM cell with peepholes
           the weights are not tied
        TODO - make peepholes optional
        TODO - work on a model with different rnn_size"""
        ## Empty List declarations
        H = list(); Wi = list(); Wf = list(); Wc = list(); Wo = list();
        input_gate = list(); forget_gate = list(); update = list(); state = list(); output_gate = list();
        for dim in range(args.num_dim):
          H.append(tf.concat(1,i_ + o_ + [state_[dim]])) # State of only 1 dimesion is used at a time
          Wi.append(tf.concat(0,ix + im + [ic[dim]]))
          Wf.append(tf.concat(0,fx + fm + [fc[dim]]))  
          Wc.append(tf.concat(0,cx + cm + [cc[dim]]))  
          Wo.append(tf.concat(0,ox + om + [oc[dim]]))  

          input_gate.append(tf.sigmoid(tf.matmul(H[dim], Wi[dim]) + ib[dim]))
          forget_gate.append(tf.sigmoid(tf.matmul(H[dim], Wf[dim]) + fb[dim]))
          update.append(tf.matmul(H[dim], Wc[dim]) + cb[dim])
          state.append(forget_gate[dim] * state_[dim] + input_gate[dim] * tf.tanh(update[dim]))
          output_gate.append(tf.sigmoid(tf.matmul(H[dim], Wo[dim]) + ob[dim]))

        return [output_gate[j] * tf.tanh(state[j]) for j in range(args.num_dim)], state

      # Input data. -- numpy array
      """
      The data input is in form of a list of lists, each of which contains  
      tf.placeholders which will act as locations for different inputs
      """
      ## train_data[dim][num_unrollings]
      self.train_data = [[tf.placeholder(tf.float32, shape=[args.batch_size,args.input_dim[dim]])                           for _ in range(args.num_unrollings[dim]+1)]                          for dim in range(args.num_dim)]
      ## Create Slices to cut of the last element in each dimension -- for train_inputs
      train_inputs = [[self.train_data[dim][time_step]                        for time_step in range(args.num_unrollings[dim])]                       for dim in range(args.num_dim)]
      ## only for character language model TODO - change this to lie in the the training loop
      ## Create Slices to cut of the first element in each dimension -- for train_labels
      # labels are inputs shifted by one time step.
      train_labels = [[self.train_data[dim][time_step]                        for time_step in range(1,args.num_unrollings[dim]+1)]                       for dim in range(args.num_dim)]
      # Unrolled LSTM loop - as a numpy array
      outputs = np.empty(tuple([args.num_unrollings[dim] for dim in range(args.num_dim)]),dtype=object)
      states = np.empty(tuple([args.num_unrollings[dim] for dim in range(args.num_dim)]),dtype=object)
      #output = saved_output
      #state = saved_state
      #pdb.set_trace()
      
      ## TODO -- Check the connections
      for index, _ in np.ndenumerate(outputs):
        ## organising inputs,outputs and states
        """
        input_ = [args.num_dim]; Choose input_ as a list for element at (i,j,k..)th time step
        output_ = np.array((args.num_dim,)); Temporary holders for output_
        state_ = np.array((args.num_dim,)); Temporary holders for state_
        """
        input_ = [train_inputs[dim][index[dim]] for dim in range(args.num_dim)]
        output_ = np.empty((args.num_dim,),dtype=object)
        state_ = np.empty((args.num_dim,),dtype=object)
        
        ## Handling end cases
        for dim in range(args.num_dim):
          if index[dim] == 0:
            output_[dim] = saved_output[dim]
            state_[dim] = saved_state[dim]
          else:
            prev_index = tuple([index[dim_] if dim_!=dim else index[dim_]-1 for dim_ in range(args.num_dim)])
            output_[dim] = outputs[prev_index][dim]
            state_[dim] = states[prev_index][dim]
        
        output = list(output_)
        state = list(state_)     
        output, state = gridlstm_cell(input_, output, state)
        outputs[index] = output
        states[index] = state
      
      # State saving across unrollings. TODO - what does assign do?
      with tf.control_dependencies([saved_output[dim].assign(output[dim]) for dim in range(args.num_dim)] +                                    [saved_state[dim].assign(state[dim]) for dim in range(args.num_dim)]):
        # Classifier.
        """
        Select the output layer -- TODO -- hard coded loss for 2d Grid to predict data along dim=1
        - write an adaptable loss function
        - write a loss function to manage > 2 dimensions
        """
        dim0 = 0
        dim1 = 1
        
        if args.loss == 'final_layer':
          outputs_ = [outputs[args.num_unrollings[dim0]-1,k][dim0] for k in range(args.num_unrollings[dim1])]
          logits = tf.nn.xw_plus_b(tf.concat(0, outputs_), w[dim0], b[dim0])
          self.loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(
              logits, tf.concat(0, train_labels[dim1])))

        if args.loss == 'diagonal':
          outputs_ = [outputs[k,k][dim0] for k in range(args.num_unrollings[dim1])]
          logits = tf.nn.xw_plus_b(tf.concat(0, outputs_), w[dim0], b[dim0])
          self.loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(
              logits, tf.concat(0, train_labels[dim1])))
          
        if args.loss == 'diagonal_sym':
          outputs_ = [outputs[k,k][dim0] for k in range(args.num_unrollings[dim1])] +          [outputs[k,k][dim1] for k in range(args.num_unrollings[dim1])]
          logits = tf.nn.xw_plus_b(tf.concat(0, outputs_), w[dim0], b[dim0])
          self.loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(
              logits, tf.concat(0, train_labels[dim1] + train_labels[dim0])))
          
        if args.loss == 'edge':
          outputs_ = [outputs[args.num_unrollings[dim0]-1,k][dim0] for k in range(args.num_unrollings[dim1])] +          [outputs[k,args.num_unrollings[dim1]-1][dim1] for k in range(args.num_unrollings[dim0])]
          logits = tf.nn.xw_plus_b(tf.concat(0, outputs_), w[dim0], b[dim0])
          self.loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(
              logits, tf.concat(0, train_labels[dim1]+train_labels[dim0])))
    
          
      # Optimizer.
      global_step = tf.Variable(0)
      self.learning_rate = tf.train.exponential_decay(
        10.0, global_step, 5000, 0.1, staircase=True)      # Learning Rate
      self.optimizer = tf.train.GradientDescentOptimizer(self.learning_rate)  # Gradient descent
      gradients, v = zip(*self.optimizer.compute_gradients(self.loss)) # Give the loss function to help find the updates
      gradients, _ = tf.clip_by_global_norm(gradients, 1.25)
      self.optimizer = self.optimizer.apply_gradients(
        zip(gradients, v), global_step=global_step)

      # Predictions.
      self.train_prediction = tf.nn.softmax(logits)

      ## TODO - will have to use unrolling in one direction - hardcoded to fit the video description problem
      ## the prediction will be along 
      ### "Re-ecoder" as specified in the gridlstm paper
      ## TODO - create a function for easy access
      # Sampling and validation eval: batch 1, no unrolling. 

      self.sample_input = [tf.placeholder(tf.float32, shape=[1, args.input_dim[dim]]) for dim in range(args.num_dim)] 
      saved_sample_output = [tf.Variable(tf.zeros([1, args.rnn_size[j]]), trainable=False) for j in range(args.num_dim)]
      saved_sample_state = [tf.Variable(tf.zeros([1, args.rnn_size[j]]), trainable=False) for j in range(args.num_dim)]
      ## Reset output, state
      reset_sample_state_assignment = tuple([saved_sample_output[dim].assign(tf.zeros([1, args.rnn_size[dim]]))                                              for dim in range(args.num_dim)] +                                             [saved_sample_state[dim].assign(tf.zeros([1, args.rnn_size[dim]]))                                              for dim in range(args.num_dim)])
      self.reset_sample_state = tf.group(*reset_sample_state_assignment)
      sample_output, sample_state = gridlstm_cell(
        self.sample_input, saved_sample_output, saved_sample_state)
      with tf.control_dependencies([saved_sample_output[dim].assign(sample_output[dim]) for dim in range(args.num_dim)] +                                    [saved_sample_state[dim].assign(sample_state[dim]) for dim in range(args.num_dim)]):
        
        self.sample_prediction = tf.nn.softmax(tf.nn.xw_plus_b(sample_output[dim0], w[dim0], b[dim0])) ## hardcoded


# In[10]:

def train(args):
  summary_frequency = 100
  ## args hardcoded
  args.input_dim = [vocabulary_size for j in range(args.num_dim)]
  
  model = Model(args)
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=args.frac_gpu)
  with tf.Session(graph=model.graph,config=tf.ConfigProto(gpu_options=gpu_options)) as session:
    tf.initialize_all_variables().run()
    print('Initialized')
    mean_loss = 0
    for step in range(args.num_epochs):
      batches = train_batches.next()
      #pdb.set_trace()
      feed_dict = dict()
      for dim in range(args.num_dim):
        for i in range(args.num_unrollings[dim] + 1):
          feed_dict[model.train_data[dim][i]] = batches[i]
      _, l, predictions, lr = session.run(
        [model.optimizer, model.loss, model.train_prediction, model.learning_rate], feed_dict=feed_dict)
      mean_loss += l
      #pdb.set_trace()
      

      ## Sampling + Saving models
      if step % summary_frequency == 0:
        if step > 0:
          mean_loss = mean_loss / summary_frequency
        # The mean loss is an estimate of the loss over the last few batches.
        print(
          'Average loss at step %d: %f learning rate: %f' % (step, mean_loss, lr))
        mean_loss = 0
        labels = np.concatenate(list(batches)[1:])
        print('Minibatch perplexity: %.2f' % float(
          np.exp(logprob(predictions[:labels.shape[0]], labels))))
        if step % (summary_frequency * 10) == 0:
          # Generate some samples.
          print('=' * 80)
          for _ in range(5):
            feed = sample(random_distribution())
            sentence = characters(feed)[0]
            model.reset_sample_state.run()
            for _ in range(79):
              ## Adding dimensions to the input
              feed_dict = dict()
              for dim_ in range(args.num_dim):
                feed_dict[model.sample_input[dim_]] = feed
                #pdb.set_trace()
              prediction = model.sample_prediction.eval(feed_dict)
              feed = sample(prediction)
              sentence += characters(feed)[0]
            print(sentence)
          print('=' * 80)
        # Measure validation set perplexity.
        model.reset_sample_state.run()
        valid_logprob = 0
        for _ in range(valid_size):
          b = valid_batches.next()
          ## Adding dimensions to the input
          feed_dict = dict()
          for dim_ in range(args.num_dim):
            feed_dict[model.sample_input[dim_]] = b[0]
          predictions = model.sample_prediction.eval(feed_dict)
          valid_logprob = valid_logprob + logprob(predictions, b[1])
        print('Validation set perplexity: %.2f' % float(np.exp(
          valid_logprob / valid_size)))


# In[11]:

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='data/tinyshakespeare',
                        help='data directory containing input.txt')
    parser.add_argument('--save_dir', type=str, default='save',
                        help='directory to store checkpointed models')
    parser.add_argument('--rnn_size', type=int ,nargs='+', default=128,
                        help='size of RNN hidden state') # TODO -- can rnn_size be different for different dims?
    parser.add_argument('--num_layers', type=int, default=2,
                        help='number of layers in the RNN')
    parser.add_argument('--model', type=str, default='lstm',
                        help='rnn, gru, lstm, gridlstm, gridgru')
    parser.add_argument('--batch_size', type=int, default=50,
                        help='minibatch size')
    parser.add_argument('--seq_length', type=int ,nargs='+', default=50,
                        help='RNN sequence length')
    parser.add_argument('--num_epochs', type=int, default=50,
                        help='number of epochs')
    parser.add_argument('--save_every', type=int, default=1000,
                        help='save frequency')
    parser.add_argument('--grad_clip', type=float, default=5.,
                        help='clip gradients at this value')
    parser.add_argument('--learning_rate', type=float, default=0.002,
                        help='learning rate')
    parser.add_argument('--decay_rate', type=float, default=0.97,
                        help='decay rate for rmsprop')
    parser.add_argument('--model_name', type=str, default='modelCpk',
                        help='checkpoint file name')
    parser.add_argument('--num_unrollings', type=int ,nargs='+', default=10,
                        help='number of time stamps')
    parser.add_argument('--num_dim', type=int, default=1,
                        help='number of dimesions of the grid lstm')
    parser.add_argument('--frac_gpu', type=float, default=0.5,
                        help='fraction of gpu to use')
    parser.add_argument('--loss', type=str, default='final_layer',
                        help='the kind of loss function')
    args = parser.parse_args()
    
    # Sanity Check
    if not isinstance(args.rnn_size,(list)):
      args.rnn_size = [args.rnn_size]
    if not isinstance(args.seq_length,(list)):
      args.seq_length = [args.seq_length]
    if not isinstance(args.num_unrollings,(list)):
      args.num_unrollings = [args.num_unrollings]
    assert len(args.rnn_size) == args.num_dim       and len(args.seq_length) == args.num_dim       and len(args.num_unrollings) == args.num_dim
    
    
    train(args)


# # In[12]:

# sys.argv = ['train.py',
#             '--data_dir','data/tinyshakespeare',
#             '--save_dir','save',
#             '--rnn_size','128',
#             '--model','gridlstm',
#             '--batch_size','64',
#             '--seq_length','50',
#             '--num_epochs','10000',
#             '--save_every','10',
#             '--grad_clip','5',
#             '--learning_rate','0.02',
#             '--decay_rate','0.97',
#             '--model_name','modelCpk',
#             '--num_unrollings','10',
#             '--num_dim','1',
#             '--frac_gpu','0.5',
#             '--loss','final_layer']


# # In[ ]:

# sys.argv = ['train.py',
#             '--data_dir','data/tinyshakespeare',
#             '--save_dir','save',
#             '--rnn_size','600','600',
#             '--model','gridlstm',
#             '--batch_size','64',
#             '--seq_length','50','10',
#             '--num_epochs','10000',
#             '--save_every','10',
#             '--grad_clip','5',
#             '--learning_rate','0.02',
#             '--decay_rate','0.97',
#             '--model_name','modelCpk',
#             '--num_unrollings','10','10',
#             '--num_dim','2',
#             '--frac_gpu','0.5',
#             '--loss','diagonal_sym']


# # In[ ]:

if __name__ == '__main__':
    main()
