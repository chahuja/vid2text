# some of the clips have missing srt files, this is used to figure out which files are those
# and store them as pickle file
import pdb
import os
import pickle as pkl

all_capt = pkl.load(open('all_captions_dict.p','rb'))
missingCaptions = []
base_path = "/multicomp/datasets"

dataset = "MontrealVideoAnnotationDataset"
video_path_base = "DVDtranscription"

for movie in os.listdir(os.path.join(base_path,dataset,video_path_base)):
    for clip in os.listdir(os.path.join(base_path,dataset,video_path_base,movie,'video')):
        try:
            temp = all_capt[clip.split('.')[0]]
        except KeyError:
            missingCaptions = missingCaptions + [clip.split('.')[0]]

pkl.dump(missingCaptions, open('missingCaptions.p','wb'))
